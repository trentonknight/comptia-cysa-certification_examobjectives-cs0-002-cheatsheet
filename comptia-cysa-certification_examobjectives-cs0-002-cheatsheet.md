# 1.0 Threat and Vulnerability Management
## 1.1 Explain the importance of threat data and intelligence.
### Intelligence sources

> Threat data, when given the appropriate context, results in the creation of threat intelligence, or the knowledge of malicious actors and their behaviors; this knowledge enables defenders to gain a better understanding of their operational environments.<a href="#r1">(Chapman & Maymí)</a>

> Threat intelligence comes in many forms and can be obtained from a number of different sources. When gathering this critical data, the security professional should always classify the information with respect to its timeliness and relevancy.<a href="#r3">(McMillan, Troy)</a>

> Although terms like threat data and threat intelligence are often used interchangeably, they’re not identical. Threat data is merely raw information about known malicious domains, URLs, IP addresses, and hash values. No context is provided. Think of them as individual puzzle pieces—important yet generic on their own. In contrast, threat intelligence is the enhanced version of threat data that has been analyzed, refined, and, by extension, creates the crucial context that organizations need to understand the threat landscape. Threat intelligence is the outcome of puzzle pieces connected to form a partial or complete picture of the puzzle.<a href="#r4">(Rogers, Bobby)</a>

> Threat intelligence gathering relies on real-world information gathering, evidence collection, and analysis. Threat intelligence can be categorized into three levels of intelligence. 

>> The first is strategic intelligence, which provides broad information about threats and threat actors allowing organizations to understand and respond to trends. 

>> Second, tactical threat intelligence includes more detailed technical and behavioral information that is directly useful to security professionals and others who are tasked with defense and response. 

>> Finally, operational threat intelligence is composed of highly detailed information allowing response to a specific threat and often includes information about where it came from, who created it or how it has changed over time, how it is delivered or how it spreads, what it attempts to do, how to remove it, and how to prevent it.<a href="#r5">(Chapple & Seidl)</a>

<details>
  <summary markdown="span">Open-source intelligence</summary>
* [https://threatfeeds.io/](https://threatfeeds.io/)


MITRE ATT&CK Techniques.
* [T1596](https://attack.mitre.org/techniques/T1596/)
* [T1593](https://attack.mitre.org/techniques/T1593/)

> From an adversary point of view, it is almost always preferable to get information about a target without directly touching it. Why? Because the less it is touched, the fewer fingerprints (or log entries) are left behind for the defenders and investigators to find. In an ideal case, adversaries gain all the information they need to compromise a target successfully without once visiting it, using OSINT techniques. Passive reconnaissance, for example, is the process by which an adversary acquires information about a target network without directly interacting with it. These techniques can be focused on individuals as well as companies. Just like individuals, many companies maintain a public face that can give outsiders a glimpse into their internal operations. In the sections that follow, we describe some of the most useful sources of OSINT with which you should be familiar.<a href="#r1">(Chapman & Maymí)</a>


> Open-source intelligence (OSINT) consists of information that is publicly available to everyone, though not everyone knows that it is available. OSINT comes from public search engines, social media sites, newspapers, magazine articles, or any source that does not limit access to that information.<a href="#r3">(McMillan, Troy)</a>

* [https://en.wikipedia.org/wiki/Open-source_intelligence](https://en.wikipedia.org/wiki/Open-source_intelligence)
* [https://osintframework.com/](https://osintframework.com/)

[Osint.org](https://osint.org/about/) provides the following definition for Open-source Intelligence (OSINT):

> Open-Source Intelligence (OSINT) is intelligence produced from publicly available information that is collected, exploited, and disseminated in a timely manner to an appropriate audience for the purpose of addressing a specific intelligence requirement. OSINT draws from a wide variety of information and sources, including the following: 
> > Mass Media: Newspapers, magazines, radio, television, and other computer-based information. 

> > Public Data: Information derived from government reports; official data, such as data on budgets and demographics; hearings; legislative debates; press conferences, speeches, directories, organizational charts, marine and aeronautical safety warnings, environmental impact statements, contract awards, and required financial disclosures, and other public sources. 

> > Gray Literature (or Grey Literature): Opensource material that usually is available through controlled access for a specific audience. Gray literature may include, but is not limited to, research reports, technical reports, economic reports, travel reports, working papers, discussion papers, unofficial government documents, proceedings, preprints, studies, dissertations and theses, trade literature, market surveys, and newsletters. The material in gray literature covers scientific, political, socioeconomic, and military disciplines. 

>  Observation and Reporting: Information of significance, not otherwise available, that is provided by, for example, amateur airplane spotters, radio monitors, and satellite observers. The availability of worldwide satellite photography, often in high resolution, on the Web (e.g., Google Earth) has expanded the public’s ability to acquire information formerly available only to major intelligence services.

[![asciicast](https://asciinema.org/a/267924.svg)](https://asciinema.org/a/267924)

[![asciicast](https://asciinema.org/a/351624.svg)](https://asciinema.org/a/351624)
</details>

<details>
  <summary markdown="span">Proprietary/closed-source intelligence</summary>


> Proprietary/closed-source intelligence sources are those that are not publicly available and usually require a fee to access. Examples of these sources are platforms maintained by private organizations that supply constantly updating intelligence information. In many cases this data is developed from all of the provider’s customers and other sources.<a href="#r3">(McMillan, Troy)</a>


> Often superior to its OSINT counterpart, closed-source intelligence involves the collection of information from restricted, covert, or fee-based sources. In other words, the information found in these sources is not available to the general public. These closed sources can range from “underground” dark web sites to classified government systems, which are only accessible to individuals with security clearances.<a href="#r4">(Rogers, Bobby)</a>

> Commercial security vendors, government organizations, and other security-centric organizations also create and use proprietary, or [closed source](https://learning.oreilly.com/library/view/comptia-cysa-study/9781119684053/c02.xhtml), intelligence. They do their own information gathering and research, and they may use custom tools, analysis models, or other proprietary methods to gather, curate, and maintain their threat feeds. There are a number of reasons that proprietary threat intelligence may be used. The organization may want to keep their threat data secret, they may want to sell or license it and their methods and sources are their trade secrets, or they may not want to take the chance of the threat actors knowing about the data they are gathering <a href="#r5">(Chapple & Seidl)</a>.

> One of the key tenets of intelligence analysis is never relying on a single source of data when attempting to confirm a hypothesis. Ideally, analysts should look at multiple artifacts from multiple sources that support a hypothesis. Similarly, open source data is best used with corroborating data acquired from closed sources. [Closed source data](https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260464313/ch1.xhtml#lev1sec2) is any data collected covertly or as a result of privileged access. Common types of closed source data include internal network artifacts, dark web communications, details from intelligence-sharing communities, and private banking and medical records. Since closed source data tends to be higher quality, an analyst can confidently assess and verify findings using any number of intelligence analysis methods and tools. An added benefit of using multiple sources is that the practice reduces the effect of confirmation bias, or the tendency for an analyst to interpret information in a way that supports a prior strongly held belief <a href="#r1">(Chapman & Maymí)</a>.


</details>

<details>
  <summary markdown="span">Timeliness</summary>

> good [threat intelligence](https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260464313/ch1.xhtml#lev1sec3) often has three characteristics: timeliness, relevancy, and accuracy <a href="#r1">(Chapman & Maymí)</a>.

> All intelligence, whether in a traditional military operation or as it applies to information security, has a <b>temporal dimension</b>. After all, intelligence considers environmental conditions as a part of context, so it makes sense that it is most useful given the time-related stipulations within the intelligence requirements. You may find that intelligence may be extremely useful at one time and completely useless at another. Accordingly, intelligence that is not delivered in a timely manner is not as useful to decision-makers <a href="#r1">(Chapman & Maymí)</a>.

> The Common Vulnerability Scoring System (CVSS) consists of three metric groups: Base, <b>Temporal</b>, and Environmental. The Base group represents the intrinsic qualities of a vulnerability that are constant over time and across user environments, the <b>Temporal</b> group reflects the characteristics of a vulnerability that change over time, and the Environmental group represents the characteristics of a vulnerability that are unique to a user's environment <a href="#r2">(Firts.org)</a>
 
<img title="MetricGroups" alt="Alt text" src="/images/MetricGroups.svg">
<img title="EquationsDiagram" alt="Alt text" src="/images/EquationsDiagram.svg">
<img title="Temporal" alt="Temporal.png" src="/images/Temporal.png">

> One of the considerations when analyzing intelligence data (of any kind, not just cyber data) is the timeliness of such data. Obviously, if an organization receives threat data that is two weeks old, quite likely it is too late to avoid that threat. One of the attractions of closed-source intelligence is that these platforms typically provide near real-time alerts concerning such threats <a href="#r3">(McMillan, Troy)</a>.

> In the context of threat intelligence, timeliness is described as a relationship between the time that threat data is collected, organized, and finally reported. Since most threat data loses value over time, data must be quickly received and acted upon to make a difference. Yet, data collected too early or too late will either tell the wrong story or tell an old one. In either case, the lack of timeliness of data will likely result in ineffective decision making and, by extension, threat mitigations <a href="#r4">(Rogers, Bobby)</a>. 

> [Timeliness](https://cve.mitre.org/docs/docs-2001/Development_of_CVE.html) of decision making is a critical lesson learned. A concern was raised that having a democratic voting process might delay processing of new entries. While timeliness is a major consideration, it should not be the overriding concern. Accuracy and completeness must be paramount. Additionally, most of the real use for CVE is involved in interoperability and information sharing. As CVE moves from validation of a large initial startup mode, to a smaller, steady stream of new candidates, the amount of time required to validate entries will diminish. As new candidates are proposed and discussed, they will be assigned a candidate number so that discussions and information sharing on a new vulnerability can continue while the decision making is underway. Also, where timeliness has been identified as an issue, the CVE Editorial Board has been responsive enough to accommodate this need. The democratic nature of CVE is preferable in an effort like this, where the real benefit is to gain broad consensus and acceptance. Should the process be an individual effort, without community participation or ownership, then there would be little probability of the CVE ever gaining the necessary acceptance to be useful.<a href="#r8">(The MITRE Corporation)</a>

</details>
<details>
  <summary markdown="span">Relevancy</summary>

> good [threat intelligence](https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260464313/ch1.xhtml#lev1sec3) often has three characteristics: timeliness, relevancy, and accuracy <a href="#r1">(Chapman & Maymí)</a>.

The NIST [Computer security resources center (CSRC) Glossary](https://csrc.nist.gov/glossary) has the following definition:

> relevant event: An occurrence (e.g., an auditable event or flag) considered to have potential security implications to the system or its environment that may require further action (noting, investigating, or reacting).
Source(s): [CNSSI 4009-2015](https://www.cnss.gov/CNSS/issuances/Instructions.cfm) 

> As discussed earlier, internal network data invariably yields the most useful threat intelligence, because it reflects the nuances of an organization. Additionally, relevancy varies based on the levels of operation, even within the same organization. It is therefore important to prepare threat intelligence products for the correct audience. Details about the exact nature of an adversary’s technical capabilities, for example, may not be as useful for a strategic audience as it may be for a detection team analyst. This is an easy point to overlook, but intelligence requires human attention to consume and process, so irrelevant information is costly in terms of time and resources. Providing inconsequential intelligence is distracting, but it may be counterproductive as well.<a href="#r1">(Chapman & Maymí)</a>


> Intelligence data can be quite voluminous. The vast majority of this information is irrelevant to any specific organization. One of the jobs of the security professional is to ascertain which data is relevant and which is not. Again, many proprietary platforms allow for searching and organizing the data to enhance its relevancy.<a href="#r3">(McMillan, Troy)</a>

> Not every organization is at risk for the same kinds of threats. For example, if your organization doesn’t use Mozilla Firefox, don’t collect threat data about it. With organizations already full to bursting with data, be sure to exercise data frugality. We should concentrate on only collecting data that enlightens us into the probabilities and impacts of threats against actual technologies used at our companies. <a href="#r4">(Rogers, Bobby)</a>

</details>

<details>
  <summary markdown="span">Accuracy</summary>

> good [threat intelligence](https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260464313/ch1.xhtml#lev1sec3) often has three characteristics: timeliness, relevancy, and accuracy <a href="#r1">(Chapman & Maymí)</a>. 

> Although it may seem self-evident in a field such as information security, accuracy is critical to enable a decision-maker to draw reliable conclusions and pursue a recommended course of action. The information must be factually correct. Acknowledging that it may be unrealistic to an exact understanding of the operational environment at any given time, an analyst must at minimum convey facts as they exist.<a href="#r1">(Chapman & Maymí)</a>

> Finally, the security professional must determine whether the intelligence is correct (accuracy). Newspapers are full these days of cases of false intelligence. The most basic example of this is the hoax email containing a false warning of a malware infection on the local device. Although the email is false, in many cases it motivates the user to follow a link to free software that actually installs malware. Again, many cyber attacks use false information to misdirect network defenses.<a href="#r3">(McMillan, Troy)</a>


> In some cases, inaccurate threat data can be worse than no threat data. Inaccuracy usually takes the form of threat data corruption, spoofing, or improper analysis, and one should not ignore the role played by false positives. False positives are inevitable, but too many indicate an ineffective threat intelligence program. Being flooded with false positives will prevent you from keying in on important threat data—and may even help create data inaccuracies by starving a system of its resources.<a href="#r4">(Rogers, Bobby)</a>


</details>


### Confidence levels

> In a continuous effort to apply more rigorous standards to analytical assessment, intelligence providers often use three levels of analytic confidence made using <i>estimative language</i>. Estimative language aims to communicate intelligence assessments while acknowledging the existence of incomplete or fragmented information. <a href="#r1">(Chapman & Maymí)</a>

> While timeliness and relevancy are key characterizes to evaluate with respect to intelligence, the security professional must also make an assessment as to the confidence level attached to the data. That is, can it be relied on to predict the future or to shed light on the past? On a more basic level, is it true? Or was the data developed to deceive or mislead? Many cyber activities have as their aim to confuse, deceive, and hide activities.<a href="#r3">(McMillan, Troy)</a>

> Once threat indicators have been analyzed, analysts will often assign them threat and confidence ratings to determine the threat’s level of nastiness and the company’s confidence in that determination. <a href="#r4">(Rogers, Bobby)</a>

> Many threat feeds will include a confidence rating, along with a descriptive scale. For example, ThreatConnect uses [six levels of confidence](https://threatconnect.com/blog/best-practices-indicator-rating-and-confidence/)<a href="#r5">(Chapple & Seidl)</a>

### Indicator management

<details>
  <summary markdown="span">Structured Threat Information eXpression (STIX)</summary>
* [https://oasis-open.github.io/cti-documentation/stix/intro](https://oasis-open.github.io/cti-documentation/stix/intro)

> [Structured Threat Information Expression (STIX™)](https://oasis-open.github.io/cti-documentation/stix/intro) is a language and serialization format used to exchange cyber threat intelligence (CTI). STIX is open source and free allowing those interested to contribute and ask questions freely.

> STIX defines a set of [STIX Domain Objects (SDOs)](https://docs.oasis-open.org/cti/stix/v2.1/os/stix-v2.1-os.html#_1j0vun2r7rgb): Attack Pattern, Campaign, Course of Action, Grouping, Identity, Indicator, Infrastructure, Intrusion Set, Location, Malware, Malware Analysis, Note, Observed Data, Opinion, Report, Threat Actor, Tool, and Vulnerability. Each of these objects corresponds to a concept commonly used in CTI.

> [STIX Cyber-observable Objects (SCOs)](https://docs.oasis-open.org/cti/stix/v2.1/os/stix-v2.1-os.html#_rosvg2qjx4h4) document the facts concerning what happened on a network or host, and do not capture the who, when, or why. By associating SCOs with STIX Domain Objects (SDOs), it is possible to convey a higher-level understanding of the threat landscape, and to potentially provide insight as to the who and the why particular intelligence may be relevant to an organization. For example, information about a file that existed, a process that was observed running, or that network traffic occurred between two IPs can all be captured as SCOs.

> In this scenario, a threat actor group named “Disco Team” is modeled using STIX Threat Actor and Identity objects. Disco Team operates primarily in Spanish and they have been known to steal credit card information for financial gain. They use the e-mail alias “disco-team@stealthemail.com” publicly and are known alternatively as [“Equipo del Discoteca”](https://oasis-open.github.io/cti-documentation/examples/identifying-a-threat-actor-profile). 

STIX v2 JSON 
```json
{
    "type": "bundle",
    "id": "bundle--601cee35-6b16-4e68-a3e7-9ec7d755b4c3",
    "objects": [
        {
            "type": "threat-actor",
            "spec_version": "2.1",
            "id": "threat-actor--dfaa8d77-07e2-4e28-b2c8-92e9f7b04428",
            "created": "2014-11-19T23:39:03.893Z",
            "modified": "2014-11-19T23:39:03.893Z",
            "name": "Disco Team Threat Actor Group",
            "description": "This organized threat actor group operates to create profit from all types of crime.",
            "threat_actor_types": [
                "crime-syndicate"
            ],
            "aliases": [
                "Equipo del Discoteca"
            ],
            "roles": [
                "agent"
            ],
            "goals": [
                "Steal Credit Card Information"
            ],
            "sophistication": "expert",
            "resource_level": "organization",
            "primary_motivation": "personal-gain"
        },
        {
            "type": "identity",
            "spec_version": "2.1",
            "id": "identity--733c5838-34d9-4fbf-949c-62aba761184c",
            "created": "2016-08-23T18:05:49.307Z",
            "modified": "2016-08-23T18:05:49.307Z",
            "name": "Disco Team",
            "description": "Disco Team is the name of an organized threat actor crime-syndicate.",
            "identity_class": "organization",
            "contact_information": "disco-team@stealthemail.com"
        },
        {
            "type": "relationship",
            "spec_version": "2.1",
            "id": "relationship--a2e3efb5-351d-4d46-97a0-6897ee7c77a0",
            "created": "2020-02-29T18:01:28.577Z",
            "modified": "2020-02-29T18:01:28.577Z",
            "relationship_type": "attributed-to",
            "source_ref": "threat-actor--dfaa8d77-07e2-4e28-b2c8-92e9f7b04428",
            "target_ref": "identity--733c5838-34d9-4fbf-949c-62aba761184c"
        }
    ]
}
```
* STIX v2 Python Producer
```python
from stix2.v21 import (ThreatActor, Identity, Relationship, Bundle)

threat_actor = ThreatActor(
    id="threat-actor--dfaa8d77-07e2-4e28-b2c8-92e9f7b04428",
    created="2014-11-19T23:39:03.893Z",
    modified="2014-11-19T23:39:03.893Z",
    name="Disco Team Threat Actor Group",
    description="This organized threat actor group operates to create profit from all types of crime.",
    threat_actor_types=["crime-syndicate"],
    aliases=["Equipo del Discoteca"],
    roles=["agent"],
    goals=["Steal Credit Card Information"],
    sophistication="expert",
    resource_level="organization",
    primary_motivation="personal-gain"
)

identity = Identity(
    id="identity--733c5838-34d9-4fbf-949c-62aba761184c",
    created="2016-08-23T18:05:49.307Z",
    modified="2016-08-23T18:05:49.307Z",
    name="Disco Team",
    description="Disco Team is the name of an organized threat actor crime-syndicate.",
    identity_class="organization",
    contact_information="disco-team@stealthemail.com"
)

relationship = Relationship(threat_actor, 'attributed-to', identity)

bundle = Bundle(objects=[threat_actor, identity, relationship])
```
* STIX v2 Python Consumer
```python
from stix2.v21 import (Bundle)

for obj in bundle.objects:
    if obj == threat_actor:
        print("------------------")
        print("== THREAT ACTOR ==")
        print("------------------")
        print("ID: " + obj.id)
        print("Created: " + str(obj.created))
        print("Modified: " + str(obj.modified))
        print("Name: " + obj.name)
        print("Description: " + obj.description)
        print("Threat Actor Types: " + str(obj.threat_actor_types))
        print("Aliases: " + str(obj.aliases))
        print("Roles: " + str(obj.roles))
        print("Goals: " + str(obj.goals))
        print("Sophistication: " + obj.sophistication)
        print("Resource Level: " + obj.resource_level)
        print("Primary Motivation: " + obj.primary_motivation)

    elif obj == identity:
        print("------------------")
        print("== IDENTITY ==")
        print("------------------")
        print("ID: " + obj.id)
        print("Created: " + str(obj.created))
        print("Modified: " + str(obj.modified))
        print("Name: " + obj.name)
        print("Description: " + obj.description)
        print("Identity Class: " + obj.identity_class)
        print("Contact Information: " + obj.contact_information)

    elif obj == relationship:
        print("------------------")
        print("== RELATIONSHIP ==")
        print("------------------")
        print("ID: " + obj.id)
        print("Created: " + str(obj.created))
        print("Modified: " + str(obj.modified))
        print("Type: " + obj.type)
        print("Relationship Type: " + obj.relationship_type)
        print("Source Ref: " + obj.source_ref)
        print("Target Ref: " + obj.target_ref)

```
> STIX 2.x introduces a top-level [Relationship object](https://oasis-open.github.io/cti-documentation/stix/compare), which links two other top-level objects via a named relationship type. STIX 2.x content can be thought of as a connected graph, where nodes are SDOs and edges are Relationship Objects. The STIX 2.x specification suggests different named relationships, but content producers are able to define their own.

```json
{
    "type": "relationship",
    "id": "relationship--01",
    "spec_version": "2.1",
    "created": "2017-02-09T11:13:27.431Z",
    "modified": "2017-02-09T11:13:27.431Z",
    "relationship_type": "uses",
    "source_ref": "attack-pattern--03",
    "target_ref": "tool--04"
 }
```

<img title="MetricGroups" alt="Alt text" align=center width="800" src="/images/NewSTIXdiagram3.PNG">


>  The Structured Threat Information Expression, or STIX, is a collaborative effort led by the MITRE Corporation to communicate threat data using a standardized lexicon. To represent threat information, the STIX 2.0 framework uses a structure consisting of twelve key STIX Domain Objects (SDOs) and two STIX Relationship Objects (SROs). In describing an event, an analyst may show the relationship between one SDO and another using an SRO. The goal is to allow for flexible, extensible exchanges while providing both human- and machine-readable data. STIX information can be represented visually for analysts or stored as JSON (JavaScript Object Notation) for use in automation.<a href="#r1">(Chapman & Maymí)</a>

> While STIX was originally sponsored by the Office of Cybersecurity and Communications (CS&C) within the U.S. Department of Homeland Security (DHS), it is now under the management of the Organization for the Advancement of Structured Information Standards (OASIS), a nonprofit consortium that seeks to advance the development, convergence, and adoption of open standards for the Internet.<a href="#r3">(McMillan, Troy)</a>

> You need a universal way to describe threat intel, so what language should you use? Enter STIX. STIX is a standardized language for describing the “what” of threat data.<a href="#r4">(Rogers, Bobby)</a>

> Structured Threat Information Expression (STIX) is an XML language originally sponsored by the U.S. Department of Homeland Security. STIX 2.0 (its current version as of this writing) defines 12 STIX domain objects, including things like attack patterns, identities, malware, threat actors, and tools. These objects are then related to each other by one of two STIX relationship object models: either as a relationship or as a sighting.<a href="#r5">(Chapple & Seidl)</a>

> STIX 2.x requires implementations to support JSON serialization, while STIX 1.x was defined using XML. Though both XML and JSON have benefits, the CTI TC determined that JSON was more lightweight, and sufficient to express the semantics of cyber threat intelligence information. It is simpler to use and increasingly preferred by developers.

* STIX v1.x Sample Object

```xml
<stix:TTPs>
 <stix:TTP id="attack-pattern:ttp-01" xsi:type='ttp:TTPType'
           version="1.1">
   <ttp:Title>Initial Compromise</ttp:Title>
    <ttp:Behavior>
     <ttp:Attack_Patterns>
      <ttp:Attack_Pattern capec_id="CAPEC-163">
       <ttp:Description>Spear Phishing</ttp:Description>
        </ttp:Attack_Pattern>
      </ttp:Attack_Patterns>
    </ttp:Behavior>
 </stix:TTP>
</stix:TTPs>
<stix:TTPs>
 <stix:Kill_Chains>
  <stixCommon:Kill_Chain id="stix:TTP-02"
                         name="mandiant-attack-lifecycle-model">
  <stixCommon:Kill_Chain_Phase name="initial-compromise"
                               phase_id="stix:TTP-03"/>
 </stix:Kill_Chains>
</stix:TTPs>

```

* STIX v2.x Sample SDO

```json
{
    "type": "attack-pattern",

    "id": "attack-pattern--01",
    "spec_version": "2.1",
    "created": "2015-05-15T09:11:12.515Z",
    "modified": "2015-05-15T09:11:12.515Z",
    "name": "Initial Compromise",
    "external_references": [
      {
        "source_name": "capec",
        "description": "spear phishing",
        "external_id": "CAPEC-163"
      }
    ],
    "kill_chain_phases": [
      {
        "kill_chain_name": "mandiant-attack-lifecycle-model",
        "phase_name": "initial-compromise"
      }
    ]
   }
```


</details>

<details>
  <summary markdown="span">Trusted Automated eXchange of Indicator Information (TAXII)</summary>

> [Trusted Automated Exchange of Intelligence Information (TAXII™)](https://oasis-open.github.io/cti-documentation/taxii/intro.html) is an application protocol for exchanging CTI over HTTPS. TAXII defines a RESTful API (a set of services and message exchanges) and a set of requirements for TAXII Clients and Servers. As depicted below, TAXII defines two primary services to support a variety of common sharing models:

>> Collection - A Collection is an interface to a logical repository of CTI objects provided by a TAXII Server that allows a producer to host a set of CTI data that can be requested by consumers: TAXII Clients and Servers exchange information in a request-response model.

>> Channel - Maintained by a TAXII Server, a Channel allows producers to push data to many consumers and consumers to receive data from many producers: TAXII Clients exchange information with other TAXII Clients in a publish-subscribe model. Note: The TAXII 2.1 specification reserves the keywords required for Channels but does not specify Channel services. Channels and their services will be defined in a later version of TAXII.


<img title="MetricGroups" alt="Alt text" align=center width="800" src="/images/taxii_diagram2.png">

##### Taxii Client Example


```bash
pip install taxii2-client
```

```python
# Performing TAXII 2.0 Requests
from taxii2client.v20 import Collection, as_pages

collection = Collection('https://example.com/api1/collections/91a7b528-80eb-42ed-a74d-c6fbd5a26116')
print(collection.get_object('indicator--252c7c11-daf2-42bd-843b-be65edca9f61'))

# For normal (no pagination) requests
print(collection.get_objects())
print(collection.get_manifest())

# For pagination requests.
# Use *args for other arguments to the call and **kwargs to pass filter information
for bundle in as_pages(collection.get_objects, per_request=50):
    print(bundle)

for manifest_resource in as_pages(collection.get_manifest, per_request=50):
    print(manifest_resource)

# ---------------------------------------------------------------- #
# Performing TAXII 2.1 Requests
from taxii2client.v21 import Collection, as_pages

collection = Collection('https://example.com/api1/collections/91a7b528-80eb-42ed-a74d-c6fbd5a26116')
print(collection.get_object('indicator--252c7c11-daf2-42bd-843b-be65edca9f61'))

# For normal (no pagination) requests
print(collection.get_objects())
print(collection.get_manifest())

# For pagination requests.
# Use *args for other arguments to the call and **kwargs to pass filter information
for envelope in as_pages(collection.get_objects, per_request=50):
    print(envelope)

for manifest_resource in as_pages(collection.get_manifest, per_request=50):
    print(manifest_resource)
```
##### Taxi Server Example
* [OpenTaxi](https://www.opentaxii.org/en/stable/)
> [Medallion](https://medallion.readthedocs.io/en/latest/) is a minimal implementation of a TAXII 2.0 Server in Python.

```bash
 pip install medallion
 medallion --port 80 --conf-file config_file.json
```

Example config_file.json using MongoDB

```json
{
     "backend": {
        "module_class": "MongoBackend",
        "uri": "<Mongo DB server url>  # e.g., 'mongodb://localhost:27017/'"
     }
}

```

> TAXII 1.0 was designed to integrate with existing sharing agreements, including access control limitations, using three primary models: hub and spoke, source/subscriber, and peer-to-peer<a href="#r1">(Chapman & Maymí)</a>

> Trusted Automated eXchange of Indicator Information (TAXII) is an application protocol for exchanging cyber threat information (CTI) over HTTPS. It defines two primary services, Collections and Channels. <a href="#r3">(McMillan, Troy)</a>

> Previous cyber threat sharing models were less efficient at sharing data. Since STIX and TAXII are machine-readable, they’re easily automated and more efficient at sharing.<a href="#r4">(Rogers, Bobby)</a>

> TAXII is intended to allow cyberthreat information to be communicated at the application layer via HTTPS. TAXII is specifically designed to support STIX data exchange. <a href="#r5">(Chapple & Seidl)</a>

</details>



<details>
  <summary markdown="span">OpenIoC</summary>
* [https://www.fireeye.com/services/freeware.html](https://www.fireeye.com/services/freeware.html)


> OpenIOC is a framework designed by Mandiant, an American cybersecurity firm that is now part of FireEye. The goal of the framework is to organize information about an attacker’s TTPs and other indicators of compromise in a machine-readable format for easy sharing and automated follow-up. The OpenIOC structure is straightforward, consisting three of main components:
>> The metadata component provides useful indexing and reference information about the IOC including author name, the IOC name, and a description of the IOC.
>> The reference component is primarily meant to enable analysts to describe how the IOC fits in operationally with their specific environments. As a result, some of the information may not be appropriate to share, because it may refer to internal systems or sensitive ongoing cases. 

>> the definition component provides the indicator content most useful for investigators and analysts. The definition often contains Boolean logic to communicate the conditions under which the IOC is valid. For example, the requirement for an MD5 hash AND a file size attribute above a certain threshold would have to be fulfilled for the indicator to match. <a href="#r1">(Chapman & Maymí)</a>

> OpenIOC (Open Indicators of Compromise) is an open framework designed for sharing threat intelligence information in a machine-readable format. It is a simple framework that is written in XML, which can be used to document and classify forensic artifacts. It comes with a base set of 500 predefined indicators, as provided by Mandiant (a U.S. cybersecurity firm later acquired by FireEye).<a href="#r3">(McMillan, Troy)</a>

> OpenIOC is written in XML and is adaptable enough to permit incident responders easy translation of threat knowledge into a standard format. Businesses use OpenIOC to share IOCs with other businesses that serve the threat intelligence communities worldwide.<a href="#r4">(Rogers, Bobby)</a>

> Another option is the Open Indicators of Compromise (OpenIOC) format. Like STIX, OpenIOC is an XML-based framework. The OpenIOC schema was developed by Mandiant, and it uses Mandiant's indicators for its base framework. A typical IOC includes metadata like the author, the name of the IOC, and a description; references to the investigation or case and information about the maturity of the IOC; and the definition for the indicator of compromise, which may include details of the actual compromise.<a href="#r5">(Chapple & Seidl)</a>

</details>

### Threat classification

<details>
  <summary markdown="span">Known threat vs. Unknown threat</summary>

> [Heuristic analysis](https://en.wikipedia.org/wiki/Heuristic_analysis) is a method employed by many computer antivirus programs designed to detect previously unknown computer viruses, as well as new variants of viruses already in the "wild".

> In attempting to find malicious activity, antivirus software and more sophisticated security devices work by using signature-based and anomaly-based methods of detection. Signature-based systems rely on prior knowledge of a threat, which means that these systems are only as good as the historical data companies have collected. Although these systems are useful for identifying threats that already exist, they don’t help much with regard to threats that constantly change form or have not been previously observed; these will slip by, undetected.
>> The alternative is to use a solution that looks at what the executable is doing, rather than what it looks like. This kind of system relies on heuristic analysis to observe the commands the executable invokes, the files it writes, and any attempts to conceal itself. Often, these heuristic systems will sandbox a file in a virtual operating system and allow it to perform what it was designed to do in that separate environment.<a href="#r1">(Chapman & Maymí)</a>

> In the cybersecurity field, known threats are threats that are common knowledge and easily identified through signatures by antivirus and intrusion detection system (IDS) engines or through domain reputation blacklists. Unknown threats, on the other hand, are lurking threats that may have been identified but for which no signatures are available.<a href="#r3">(McMillan, Troy)</a>

> Signature-based detection involves the detection of attacks by looking for specific known types of information unique to a threat, such as network traffic characteristics or malware code, and comparing its “signature” with a well-known database of malicious signatures. 
>> Heuristic analysis can work *statically*, where suspected code is decompiled into source code for analysis, or *dynamically*, where the suspected code is isolated into a virtual environment where it can run in real time and be analyzed with less risk. <a href="#r4">(Rogers, Bobby)</a>

<table align=center> 
<tr> 
<th>Category</th> 
<th>Description <a href="#r4">(Rogers, Bobby)</a></th>
</tr>
<tr>
<td>Known knowns</td>
<td>Threats we'er aware of and understand</td> 
</tr>
<tr>
<td>Known unknowns</td>
<td>Threats we'er aware of but don't understand</td>
</tr>
<tr>
<td>Unknown knowns</td>
<td>Threats we understand but are not aware of</td>
</tr>
<tr>
<td>Unknown unknowns</td>
<td>Threats we neither aware of nor understand</td>
</tr>
</table>

</details>

<details>
  <summary markdown="span">Zero-day</summary>

* [https://www.zerodayinitiative.com/about/benefits/](https://www.zerodayinitiative.com/about/benefits/)

* [https://hackerone.com/bug-bounty-programs](https://hackerone.com/bug-bounty-programs)

> A zero-day (also known as 0-day) is a computer-software vulnerability either unknown to those who should be interested in its mitigation (including the vendor of the target software) or known and a patch has not been developed. Until the vulnerability is mitigated, hackers can exploit it to adversely affect programs, data, additional computers or a network. An exploit directed at a zero-day is called a zero-day exploit, or zero-day attack. <a href="#w1">(Zero-day (computing))</a>

> A zero-day vulnerability is a flaw in a piece of software that the vendor is unaware of and thus has not issued a patch or advisory for. The code written to take advantage of this flaw is called the zero-day exploit. <a href="#r1">(Chapman & Maymí)</a>

> Honeypots or honeynets can also provide forensic information about hacker methods and tools for zero-day attacks. New zero-day attacks against a broad range of technology systems are announced on a regular basis. <a href="#r3">(McMillan, Troy)</a>

> When a vendor stops supporting a product—as in Microsoft’s case with Windows 7—any vulnerabilities discovered thereafter will remain indefinitely. That’s a major problem because a lot of businesses don’t want to, or cannot, move on from Windows 7 for the foreseeable future.<a href="#r4">(Rogers, Bobby)</a>

</details>

<details>
  <summary markdown="span">Advanced persistent threat</summary>

MITRE ATT&CK provides detailed information of Advanced Persistent Threats within [`Groups`](https://attack.mitre.org/groups/):

> Groups are sets of related intrusion activity that are tracked by a common name in the security community. Analysts track clusters of activities using various analytic methodologies and terms such as threat groups, activity groups, threat actors, intrusion sets, and campaigns. Some groups have multiple names associated with similar activities due to various organizations tracking similar activities by different names. Organizations' group definitions may partially overlap with groups designated by other organizations and may disagree on specific activity.

One example group is APT29 with ID: [G0016](https://attack.mitre.org/groups/G0016/). MITRE ATT&CK uses Tactics Techniques and Procedures (TTP). In the below example: 
- `Tactic` *Privilege Escalation* [TA0004](https://attack.mitre.org/tactics/TA0004/)
- `Technique` *Abuse Elevation Control Mechanism* [T1548](https://attack.mitre.org/techniques/T1548/)
- `Procedure` *Exaramel for Linux* [S0401](https://attack.mitre.org/software/S0401/). 

> APT29 is threat group that has been attributed to Russia's Foreign Intelligence Service (SVR).They have operated since at least 2008, often targeting government networks in Europe and NATO member countries, research institutes, and think tanks. APT29 reportedly compromised the Democratic National Committee starting in the summer of 2015.

One of the many Tactics used by APT29 are Privilege Escalation [TA0004](https://attack.mitre.org/tactics/TA0004/) and Defense Evasion [TA0005](https://attack.mitre.org/tactics/TA0005/). APT29 is specifically uses the, `Abuse Elevation Control Mechanism` [T1548](https://attack.mitre.org/techniques/T1548/).

> Adversaries may circumvent mechanisms designed to control elevate privileges to gain higher-level permissions. Most modern systems contain native elevation control mechanisms that are intended to limit privileges that a user can perform on a machine. Authorization has to be granted to specific users in order to perform tasks that can be considered of higher risk. An adversary can perform several methods to take advantage of built-in control mechanisms in order to [escalate privileges on a system](https://attack.mitre.org/techniques/T1548/).

A Sub-technique of `Abuse Elevation Control Mechanism` [T1548](https://attack.mitre.org/techniques/T1548/) is [Abuse Elevation Control Mechanism](https://attack.mitre.org/techniques/T1548/001/): Setuid and Setgid. 

> An adversary may perform shell escapes or exploit vulnerabilities in an application with the setsuid or setgid bits to get code running in a different user’s context. On Linux or macOS, when the setuid or setgid bits are set for an application, the application will run with the privileges of the owning user or group respectively. [1]. Normally an application is run in the current user’s context, regardless of which user or group owns the application. However, there are instances where programs need to be executed in an elevated context to function properly, but the user running them doesn’t need the elevated privileges.

>> Instead of creating an entry in the sudoers file, which must be done by root, any user can specify the setuid or setgid flag to be set for their own applications. These bits are indicated with an "s" instead of an "x" when viewing a file's attributes via ls -l. The chmod program can set these bits with via bitmasking, chmod 4777 [file] or via shorthand naming, chmod u+s [file]. Adversaries can use this mechanism on their own malware to make sure they're able to execute in elevated contexts in the future.

The MITRE ATT&CK frameworks lists `Exaramel for Linux` [S0401](https://attack.mitre.org/software/S0401/) as one procedure used for accomplishing [Abuse Elevation Control Mechanism](https://attack.mitre.org/techniques/T1548/001/): Setuid and Setgid.

> In 2003, analysts discovered a series of coordinated attacks against the Department of Defense, Department of Energy, NASA, and the Department of Justice. Discovered to have been in progress for at least three years by that point, the actors appeared to be on a mission and took extraordinary steps to hide evidence of their existence. These events, known later as “Titan Rain,” would be classified as the work of an advanced persistent threat (APT), which refers to any number of stealthy and continuous computer hacking efforts, often coordinated and executed by an organization or government with significant resources.<a href="#r1">(Chapman & Maymí)</a>

While no defensive actions are 100% effective, the following actions may help mitigate many APTs:
- Use application whitelisting to help prevent malicious software and unapproved programs from running.
- Patch applications such as Java, PDF viewers, Flash, web browsers, and Microsoft Office products.
- Patch operating system vulnerabilities.
- Restrict administrative privileges to operating systems and applications, based on user duties. <a href="#r3">(McMillan, Troy)</a>

Here is the lifecycle for APTs:

1.   Define target.
2.   Find and organize accomplices.
3.   Build or acquire tools.
4.   Research target infrastructure/employees.
5.   Test for detection.
6.   Deployment.
7.   Initial intrusion.
8.   Outbound connection initiated.
9.   Expand access and obtain credentials.
10.  Strengthen foothold.
11.  Exfiltrate data.
12.  Cover tracks and remain undetected. <a href="#r4">(Rogers, Bobby)</a>

</details>

### Threat actors
<details>
  <summary markdown="span">Nation-state</summary>

- [https://us-cert.cisa.gov/china](https://us-cert.cisa.gov/china)
- [https://us-cert.cisa.gov/russia](https://us-cert.cisa.gov/russia)
- [https://us-cert.cisa.gov/northkorea](https://us-cert.cisa.gov/northkorea)
- [https://us-cert.cisa.gov/iran](https://us-cert.cisa.gov/iran)


> There are a few interesting notes about nation-state operations that make them unique. The first is that, depending on the countries involved, businesses can quickly become a part of the activity in either a direct or supporting capacity. Second, more sophisticated threat actors may incorporate false flag techniques, performing activities that lead defenders to falsely attribute their activity to another. Given the high degree of coordination that some nation-state actor activities require, this is becoming a frequent challenge for defenders to address. Finally, there’s an aspect of perspective worth noting here: one nation’s intelligence apparatus is another nation’s malicious actor.<a href="#r1">(Chapman & Maymí)</a>

> Nation-state or state sponsors are usually foreign governments. They are interested in pilfering data, including intellectual property and research and development data, from major manufacturers, tech companies, government agencies, and defense contractors. They have the most resources and are the best organized of any of the threat actor groups.<a href="#r3">(McMillan, Troy)</a>

> The goal of nation-states is typically to get any information that can give them an advantage over another country, whether it is militarily or economically. Examples would be any information regarding national defense, trade secrets or proprietary data, economic data, and even data used to blackmail individuals in other countries.<a href="#r4">(Rogers, Bobby)</a>

> Nation-state threat actors have the resources of a country behind them, and their goals are typically those of the country they are sponsored by. Nation- state actors are often associated with advanced persistent threat (APT) organizations, and they have advanced tools and capabilities not commonly seen in the hands of other threat actors.<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Hacktivist</summary>

- [https://en.wikipedia.org/wiki/Hacktivism](https://en.wikipedia.org/wiki/Hacktivism)

> Hacktivists are threat actors that typically operate with less resourcing than their nation-state counterparts, but nonetheless work to coordinate efforts to bring light to an issue or promote a cause. They often rely on readily available tools and mass participation to achieve their desired effects against a target. Though not always the case, their actions often have little lasting damage to their targets. Hacktivists are also known to use social media and defacement tactics to affect the reputation of their targets, hoping to erode public trust and confidence in their targets. Unlike other threat actors, hacktivists rarely seek to operate with stealth and look to bring attention to their cause along with notoriety for their own organization.<a href="#r1">(Chapman & Maymí)</a>

> While not mentioned by the FBI, hacktivists are activists for a cause, such as animal rights, that use hacking as a means to get their message out and affect the businesses that they feel are detrimental to their cause.<a href="#r3">(McMillan, Troy)</a>

> History has seen many notable hacktivist individuals and groups, but perhaps two of the more “mainstream” variety deserve some mention:
- Edward Snowden   Formerly a CIA employee, Snowden became notorious for leaking highly classified NSA information due to his allegation that the NSA performed abusive surveillance practices domestically and abroad.
- WikiLeaks   This whistleblowing organization, created by Julian Assange, maintains a website that publishes secret or classified materials in an effort to “fight societal corruption.”<a href="#r4">(Rogers, Bobby)</a>

> Hacktivists range from individual actors to large groups like Anonymous, and their technical capabilities and resources can vary greatly. When you are assessing threats from hacktivists, you need to carefully consider what types of hacktivists are most likely to target your organization and why.<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Organized crime</summary>

> When compared to nation-state actors, organized crime may have a more moderate sophistication level, but as financial gain is often the goal, attacks will many times include the use of cryptojacking, ransomware, and bulk data exfiltration techniques.<a href="#r1">(Chapman & Maymí)</a>

> Organized crime groups primarily threaten the financial services sector and are expanding the scope of their attacks. They are well financed and organized.<a href="#r3">(McMillan, Troy)</a>

> Organized crime groups are known for conducting widespread ransomware operations, cryptojacking, bribery, and blackmail, and they have a particular affinity for hustling millions of dollars out of duped CEOs.<a href="#r4">(Rogers, Bobby)</a>

> Organized crime has played a significant role as a threat actor, with focused attacks typically aimed at financial gain. Ransomware attacks are an increasingly common example of this type of threat from organized crime groups.<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Insider threat</summary>

> The [Cyber and Infrastructure Security Agency (CISA)](https://www.cisa.gov/defining-insider-threats) defines insider threat as the threat that an insider will use his or her authorized access, wittingly or unwittingly, to do harm to the Department’s mission, resources, personnel, facilities, information, equipment, networks, or systems. This threat can manifest as damage to the Department through the following insider behaviors:
- Espionage
- Terrorism
- Unauthorized disclosure of information
- Corruption, including participation in transnational organized crime
- Sabotage
- Workplace violence
- Intentional or unintentional loss or degradation of departmental resources or capabilities

> The insider threat can be either unintentional or intentional.[CISA](https://www.cisa.gov/defining-insider-threats)

#### Other Threats
- Collusive Threats – A subset of malicious insider threats is collusive threats, where one or more insiders collaborate with an external threat actor to compromise an organization. These incidents frequently involve cybercriminals recruiting an insider or several insiders to enable fraud, intellectual property theft, espionage, or a combination of the three.
- Third-Party Threats – Additionally, third-party threats are typically contractors or vendors who are not formal members of an organization, but who have been granted some level of access to facilities, systems, networks, or people to complete their work. These threats may be direct or indirect threats.
! Direct threats are individuals who act in a way that compromises the targeted organization.
! Indirect threats are generally flaws in systems that expose resources to unintentional or malicious threat actors. 


> To address internal threats, it’s critical that the security program is designed in a way that adheres to the principle of least privilege, as it relates to access. Furthermore, network security devices should be configured to allow or deny access based on robust access control rules and not simply as a result of a device’s location within the network. <a href="#r1">(Chapman & Maymí)</a>

> An organization should implement the appropriate event collection and log review policies to provide the means to detect insider threats as they occur. <a href="#r3">(McMillan, Troy)</a>

> Although not the most common, *compromised* insiders are compelled against their will by an external threat actor. These individuals are manipulated by the external threat into performing various theft and sabotage exercises on their behalf.<a href="#r4">(Rogers, Bobby)</a>

> In other cases, *professional insiders* were installed by, perhaps, a state-sponsor, competitor, or organized crime group for espionage purposes.<a href="#r4">(Rogers, Bobby)</a>

> Insider threats are threats from employees or other trusted individuals or groups inside an organization. They may be intentional or unintentional, but in either case, they can pose a significant threat due to the trusted position they have. Insider threats are frequently considered to be one of the most likely causes of breaches and are often difficult to detect.<a href="#r5">(Chapple & Seidl)</a>

<details>
  <summary markdown="span">Intentional</summary>

> Intentional threats are actions taken to harm an organization for personal benefit or to act on a personal grievance. The intentional insider is often synonymously referenced as a “malicious insider.” The motivation is personal gain or harming the organization. For example, many insiders are motivated to “get even” due to unmet expectations related to a lack of recognition (e.g., promotion, bonuses, desirable travel) or even termination. Their actions include leaking sensitive information, harassing associates, sabotaging equipment, or perpetrating violence. Others have stolen proprietary data or intellectual property in the false hope of advancing their careers.[CISA](https://www.cisa.gov/defining-insider-threats)

</details>
<details>
  <summary markdown="span">Unintentional</summary>

> Negligence – An insider of this type exposes an organization to a threat through carelessness. Negligent insiders are generally familiar with security and/or IT policies but choose to ignore them, creating risk for the organization. Examples include allowing someone to “piggyback” through a secure entrance point, misplacing or losing a portable storage device containing sensitive information, and ignoring messages to install new updates and security patches.[CISA](https://www.cisa.gov/defining-insider-threats)

> Accidental – An insider of this type mistakenly causes an unintended risk to an organization. Organizations can successfully work to minimize accidents, but they will occur; they cannot be completely prevented, but those that occur can be mitigated. Examples include mistyping an email address and accidentally sending a sensitive business document to a competitor, unknowingly or inadvertently clicking on a hyperlink, opening an attachment that contains a virus within a phishing email, or improperly disposing of sensitive documents.[CISA](https://www.cisa.gov/defining-insider-threats)

</details>
</details>


### Intelligence cycle

```mermaid
flowchart LR
    Requirements --> Collection --> Analysis --> Dissemination --> Feedback --> Requirements
```
<details>
  <summary markdown="span">Requirements</summary>

> In simple terms, requirements are steps that are needed. The results of this phase are not always derived from authority, but are determined by aspects of the customer’s operations as well as the capabilities of the intelligence team. As gaps in understanding are identified and prioritized, analysts will move on to figuring out ways to close these gaps, and a plan is set forth as to how they will get the data they need. This in turn will drive the collection phase.<a href="#r1">(Chapman & Maymí)</a>

> The amount of potential information may be so vast that unless we filter it to what is relevant, we may be unable to fully understand what is occurring in the environment.<a href="#r3">(McMillan, Troy)</a>

> The following requirements will need to be defined to achieve the goals: <a href="#r4">(Rogers, Bobby)</a>

•   Team roles and responsibilities

•   Resources allocated to team members

•   Timelines for meeting objectives

•   Prioritization of assets, risks, and threats

•   Sources for threat intelligence

•   Determination of threat intelligence types

•   Tools/techniques needed to collect, analyze, and report cybersecurity intelligence


</details>


<details>
  <summary markdown="span">Collection</summary>

> Unlike a collection effort at a traditional intelligence setting, this effort at a business will likely not involve dispatching of HUMINT or SIGINT assets, but will instead mean the instrumentation of technical collection methods, such as setting up a network tap or enabling enhanced logging on certain devices.<a href="#r1">(Chapman & Maymí)</a>

> Collection is time-consuming work that involves web searches, interviews, identifying sources, and monitoring, to name a few activities. New tools automate data searching, organizing, and presenting information in easy-to-view dashboards.<a href="#r3">(McMillan, Troy)</a>

> We’ll use a range of tools to collect threat data, including the following:<a href="#r4">(Rogers, Bobby)</a>

•   Security information event management (SIEM)

•   Threat intelligence platforms

•   Threat intelligence providers

•   User behavior analytics (UBA)

•   Network traffic analysis tool

•   Cybersecurity communities

The following figure shows a typical architecture of information flow in an Internet Protocol Flow Information Export (IPFIX) architecture. The IPFIX standard defines how IP flow information is to be formatted and transferred from an exporter to a collector.[1] Previously many data network operators were relying on Cisco Systems' proprietary NetFlow technology for traffic flow information export. The IPFIX standards requirements were outlined in the original RFC 3917. Cisco NetFlow Version 9 was the basis for IPFIX. The basic specifications for IPFIX are documented in RFC 7011 through RFC 7015, and RFC 5103. 

```ascii
                       Exporter      IPFIX         Collector
                          O--------------------------->O
                          |
                          | Observation
                          | Domain
                          |
       Metering #1        | Metering #2
         O----------------O----------------O Metering #3
         |                |                |
         | Observation    | Observation    | Observation
         | Point #1       | Point #2       | Point #3
         v                |                |
---- IP Traffic --->      |                |
                          v                |
--------------- More IP Traffic --->       |
                                           v
---------------------------------- More IP Traffic --->
```


</details>


<details>
  <summary markdown="span">Analysis</summary>

> Analysis is the act of making sense of what you observe. With the use of automation, highly trained analysts will try to give meaning to the normalized, decrypted, or otherwise processed information by adding context about the operational environment. They then prioritize it against known requirements, improve those requirements, and potentially identify new collection sources.<a href="#r1">(Chapman & Maymí)</a>

> In this stage, data is combed and analyzed to identify pieces of information that have the following characteristics:
1. Timely: Can be tied to the issue from a time standpoint
2. Actionable: Suggests or leads to a proper mitigation
3. Consistent: Reduces uncertainty surrounding an issue

> This is the stage in which the skills of the security professional have the most impact, because the ability to correlate data with issues requires keen understanding of vulnerabilities, their symptoms, and solutions.<a href="#r3">(McMillan, Troy)</a>

> Analysis helps us to determine the significance and implications of the data, such as the following:<a href="#r4">(Rogers, Bobby)</a>

•   Does the data show indicators of compromise?

•   Does the data show that we’re being targeted?

•   What threat predictions can we draw from the data?

•   What threat solutions should we consider implementing to quell the threats?

</details>


<details>
  <summary markdown="span">Dissemination</summary>

> Distributing the requested intelligence to the customer occurs at the dissemination phase. Intelligence is communicated in whichever manner was previous identified in the requirements phase and must provide a clear way forward for the customer. <a href="#r1">(Chapman & Maymí)</a>

> These solutions, be they policies, scripts, or configuration changes, must be communicated to the proper personnel for deployment. The security professional acts as the designer and the network team acts as the builder of the solution. In the case of policy changes, the human resources (HR) team acts as the builder.<a href="#r3">(McMillan, Troy)</a>

> With the data carefully analyzed, we must disseminate that information to the stakeholders.<a href="#r4">(Rogers, Bobby)</a>

</details>


<details>
  <summary markdown="span">Feedback</summary>

> Once intelligence is disseminated, more questions may be raised, which leads to additional planning and direction of future collection efforts.<a href="#r1">(Chapman & Maymí)</a>

> Gathering feedback on the intelligence cycle before the next cycle begins is important so that improvements can be defined. What went right? What worked? What didn’t? Was the analysis stage performed correctly? Was the dissemination process clear and timely? Improvements can almost always be identified.<a href="#r3">(McMillan, Troy)</a>

> The final phase of the intelligence cycle is feedback. As you can imagine, the producers of the intelligence (us) and the consumers of the intelligence (stakeholders) should discuss how well the intelligence efforts have met stakeholder requirements. Whether the requirements are sufficiently met or not, you can make some changes for subsequent threat collection and mitigation cycles. New requirements may arise, threat data collected may change, and new analysis and dissemination techniques may be needed to ensure requirements are better met going forward.<a href="#r4">(Rogers, Bobby)</a>

</details>

### Commodity malware

> Commodity malware includes any pervasive malicious software that’s made available to threat actors via sale. Often made available in underground communities, this type of malware enables criminals to focus less on improving their technical sophistication and more on optimizing their illegal operations.<a href="#r1">(Chapman & Maymí)</a>

> Although no clear dividing line exists between commodity malware and what is called advanced malware (and in fact the lines are blurring more all the time), generally we can make a distinction based on the skill level and motives of the threat actors who use the malware. Less-skilled threat actors (script kiddies, etc.) utilize these prepackaged commodity tools, whereas more-skilled threat actors (APTs, etc.) typically customize their attack tools to make them more effective in a specific environment. <a href="#r3">(McMillan, Troy)</a>

> Ransomware like WannaCry is a good example of commodity malware due to its use all over the world across all major sectors and industries.<a href="#r4">(Rogers, Bobby)</a>

### Information sharing and analysis communities

Information sharing communities were created to make threat data and best practices more accessible by lowering the barrier to entry and standardizing how threat information is shared and stored between organizations. While information sharing occurs frequently between industry peers, it’s usually in an informal and ad hoc fashion. One of the most effective formal methods of information sharing comes through information sharing and analysis centers (ISACs). 

> The [ISAO Standards Organization](https://www.isao.org/) is a non-governmental organization established October 1, 2015, and led by the University of Texas at San Antonio (UTSA). Our mission is to improve the Nation’s cybersecurity posture by identifying standards and guidelines for robust and effective information sharing and analysis related to cybersecurity risks, incidents, and best practices.

> The CySA+ exam objectives specifically mention three areas in this information sharing grouping: healthcare, financial services, and aviation. There are actually 25 organizations currently in the national council of ISACs, which means there is a good chance your industry may have an ISAC that covers it. You can find specific information about the three ISACs that the exam objectives mention at these sites: The healthcare ISAC, `H-ISAC`: h-isac.org, The financial services ISAC `F-ISAC`: fsisac.com, The aviation ISAC `A-ISAC`: a-isac.com <a href="#r5">(Chapple & Seidl)</a>

<details>
  <summary markdown="span">Healthcare</summary>


> The [ISAO Standards Organization](https://www.isao.org/) is a non-governmental organization established October 1, 2015, and led by the University of Texas at San Antonio (UTSA). Our mission is to improve the Nation’s cybersecurity posture by identifying standards and guidelines for robust and effective information sharing and analysis related to cybersecurity risks, incidents, and best practices.

> [Health-ISAC Inc. (H-ISAC, Health Information Sharing and Analysis Center)](https://h-isac.org/), is a global, non-profit, member-driven organization offering healthcare stakeholders a trusted community and forum for coordinating, collaborating and sharing vital physical and cyber threat intelligence and best practices with each other.

>  In the healthcare community, where protection of patient data is legally required by the Health Insurance Portability and Accountability Act (HIPAA), an example of a sharing platform is the Health Information Sharing and Analysis Center (H-ISAC). <a href="#r3">(McMillan, Troy)</a>

> Healthcare organizations would be well-served to make frequent use of the Health Information Sharing and Analysis Center (H-ISAC) to obtain updated health-related threat intelligence. <a href="#r4">(Rogers, Bobby)</a>

</details>


<details>
  <summary markdown="span">Financial</summary>

  * [https://en.wikipedia.org/wiki/Gramm%E2%80%93Leach%E2%80%93Bliley_Act](https://en.wikipedia.org/wiki/Gramm%E2%80%93Leach%E2%80%93Bliley_Act)

> The [ISAO Standards Organization](https://www.isao.org/) is a non-governmental organization established October 1, 2015, and led by the University of Texas at San Antonio (UTSA). Our mission is to improve the Nation’s cybersecurity posture by identifying standards and guidelines for robust and effective information sharing and analysis related to cybersecurity risks, incidents, and best practices.

> The [Financial Services Information Sharing and Analysis Center](https://www.fsisac.com/) (F-ISAC) is the only global cyber intelligence sharing community solely focused on financial services. Serving financial institutions and in turn their customers, the organization leverages its intelligence platform, resiliency resources, and a trusted peer-to-peer network of experts to anticipate, mitigate and respond to cyber threats.

> The [Gramm-Leach-Bliley Act](https://www.ftc.gov/tips-advice/business-center/privacy-and-security/gramm-leach-bliley-act) requires financial institutions – companies that offer consumers financial products or services like loans, financial or investment advice, or insurance – to explain their information-sharing practices to their customers and to safeguard sensitive data.

> The financial services sector is under pressure to protect financial records with laws such as the Financial Services Modernization Act of 1999, commonly known as the Gramm-Leach-Bliley Act (GLBA). The Financial Services Information Sharing and Analysis Center (FS-ISAC) is an industry consortium dedicated to reducing cyber risk in the global financial system. It shares among its members and trusted sources critical cyber intelligence, and builds awareness through summits, meetings, webinars, and communities of interest.<a href="#r3">(McMillan, Troy)</a>

> The Gramm–Leach–Bliley Act (GLBA) of 1999 plays an important role in the financial industry. It requires financial organizations to disclose how they share and protect their customers’ private information.<a href="#r4">(Rogers, Bobby)</a>


</details>


<details>
  <summary markdown="span">Aviation</summary>

> The [ISAO Standards Organization](https://www.isao.org/) is a non-governmental organization established October 1, 2015, and led by the University of Texas at San Antonio (UTSA). Our mission is to improve the Nation’s cybersecurity posture by identifying standards and guidelines for robust and effective information sharing and analysis related to cybersecurity risks, incidents, and best practices.

> [A-ISAC](https://www.a-isac.com/aboutus) Our threat intelligence represents the collective input of hundreds of analysts across the aviation sector. We act as an extension of your team, seeking member-targeted threat indicators. Information sharing occurs across multiple channels, including a secure threat intelligence platform where members have access to IOCs, APT data, campaign activity, mitigation strategies, reports, discussion forums, and more. We provide strategic analysis and identify correlations, providing actionable threat intel without the noise. 

> In the area of aviation, the U.S. Department of Homeland Security’s Cybersecurity and Infrastructure Security Agency (CISA) maintains a number of chartered organizations, among them the Aviation Government Coordinating Council (AGCC). Its charter document reads “The AGCC coordinates strategies, activities, policy and communications across government entities within the Aviation Sub-Sector. The AGCC acts as the government counterpart to the private industry-led ‘Aviation Sector Coordinating Council’ (ASCC).” The Aviation Sector Coordinating Council is an example of a private sector counterpart.<a href="#r3">(McMillan, Troy)</a>

> Aviation cybersecurity concerns are significant enough that multiple ISACs have sprouted around the globe. Of particular note is the A-ISAC, which, like most other ISACs, seeks to exchange threats, vulnerabilities, incidents, and best practices with its worldwide constituents. Europe has its own aviation-focused ISAC as well, called the European Centre for Cyber Security in Aviation (ECCSA). However, there is some concern that having multiple aviation ISACs might cause standardization challenges.<a href="#r4">(Rogers, Bobby)</a>

> The CySA+ exam objectives specifically mention three areas in this information sharing grouping: healthcare, financial services, and aviation. There are actually 25 organizations currently in the national council of ISACs, which means there is a good chance your industry may have an ISAC that covers it. You can find specific information about the three ISACs that the exam objectives mention at these sites: The healthcare ISAC, H-ISAC: h-isac.org, The financial services ISAC: fsisac.com, The aviation ISAC: a-isac.com <a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Government</summary>

> The [ISAO Standards Organization](https://www.isao.org/) is a non-governmental organization established October 1, 2015, and led by the University of Texas at San Antonio (UTSA). Our mission is to improve the Nation’s cybersecurity posture by identifying standards and guidelines for robust and effective information sharing and analysis related to cybersecurity risks, incidents, and best practices.

<a href="#r1">(Chapman & Maymí)</a>

> For government agencies, the aforementioned CISA also shares information with state, local, tribal, and territorial governments and with international partners, as cybersecurity threat actors are not constrained by geographic boundaries. As CISA describes itself on the Department of Homeland Security website, “CISA is the Nation’s risk advisor, working with partners to defend against today’s threats and collaborating to build more secure and resilient infrastructure for the future.”<a href="#r3">(McMillan, Troy)</a>

> <a href="#r4">(Rogers, Bobby)</a>
> <a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Critical Infastructure</summary>

> The [ISAO Standards Organization](https://www.isao.org/) is a non-governmental organization established October 1, 2015, and led by the University of Texas at San Antonio (UTSA). Our mission is to improve the Nation’s cybersecurity posture by identifying standards and guidelines for robust and effective information sharing and analysis related to cybersecurity risks, incidents, and best practices.


<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
> All of the previously mentioned platforms and organizations are dedicated to helping organizations protect their critical infrastructure. As an example of international cooperation, the European Union Agency for Network and Information Security (ENISA) is a center of network and information security expertise for the European Union (EU). ENISA describes itself as follows: “ENISA works with these groups to develop advice and recommendations on good practice in information security. It assists member states in implementing relevant EU legislation and works to improve the resilience of Europe’s critical information infrastructure and networks. ENISA seeks to enhance existing expertise in member states by supporting the development of cross-border communities committed to improving network and information security throughout the EU.” More information about ENISA and its work can be found at https://www.enisa.europa.eu.<a href="#r3">(McMillan, Troy)</a>

> The healthcare, financial, aviation, government, and many more industries all contain critical infrastructure. However, the CySA+ exam also considers critical infrastructure in terms of utility and public organizations such as electricity, nuclear, oil and gas, public transit, and water. Shown here are the ISACs for those respective industries <a href="#r4">(Rogers, Bobby)</a>:

•   Electricity ISAC (E-ISAC)

•   Nuclear ISAC (NEI)

•   Oil and gas (ONG-ISAC)

•   Public transit (PT-ISAC)

•   Water ISAC (Water-ISAC)

</details>

## 1.2 Given a scenario, utilize threat intelligence to support organizational security.
### Attack frameworks

<details>
  <summary markdown="span">MITRE ATT&CK</summary>

  * [https://gitlab.com/trentonknight/pentest_cheat_sheet/-/blob/master/DETTECT_Walkthrough.md](https://gitlab.com/trentonknight/pentest_cheat_sheet/-/blob/master/DETTECT_Walkthrough.md)
  * [https://github.com/rabobank-cdc/DeTTECT](https://github.com/rabobank-cdc/DeTTECT)
  * [https://redteam.guide/docs/concepts/mitre_attack/](https://redteam.guide/docs/concepts/mitre_attack/)

> [MITRE ATT&CK®](https://attack.mitre.org/) is a globally-accessible knowledge base of adversary tactics and techniques based on real-world observations. The ATT&CK knowledge base is used as a foundation for the development of specific threat models and methodologies in the private sector, in government, and in the cybersecurity product and service community.

> Beginning in 2013, MITRE began development on a model that would allow US government agencies and industry security teams to share information about attacker TTPs with one another in an effective manner. The model would come to be known as the Adversarial Tactics, Techniques, and Common Knowledge (ATT&CK) framework. <a href="#r1">(Chapman & Maymí)</a>

> MITRE ATT&CK is a knowledge base of adversary tactics and techniques based on real-world observations. It is an open system, and attack matrices based on it have been created for various industries. It is designed as a foundation for the development of specific threat models and methodologies in the private sector, in government, and in the cybersecurity product and service community.<a href="#r3">(McMillan, Troy)</a>

> The ATT&CK “tactics” describe the why of an adversary’s attack, and the “techniques” describe the how for achieving the tactic’s goal. For example, the adversary’s tactic might be Execution, and the technique could be PowerShell.<a href="#r4">(Rogers, Bobby)</a>

> MITRE provides the ATT&CK, or Adversarial Tactics, Techniques, and Common Knowledge, knowledge base of adversary tactics and techniques. <a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">The Diamond Model of Intrusion Analysis</summary>

- [The Diamond Model of Intrusion Analysis](https://apps.dtic.mil/dtic/tr/fulltext/u2/a586960.pdf)

- The `infrastructure` feature describes the physical and/or logical communication structures
the adversary uses to deliver a capability, maintain control of capabilities (e.g., command-
and-control/C2), and effect results from the victim (e.g., exfiltrate data).
- An `adversary` is the actor/organization responsible for utilizing a capability against the
victim to achieve their intent. 
- A `victim` is the target of the adversary and against whom vulnerabilities and exposures
are exploited and capabilities used.
- The `capability` feature describes the tools and/or techniques of the adversary used in the
event.

```mermaid
flowchart LR
    Infastructure --- Adversary & Victim --- Capability
```

> The components are represented as vertices on a diamond, with connections between them. Using the connections between these entities, you can use the model to describe how an adversary uses a capability in an infrastructure against a victim. A key feature of the model is that enables defenders to pivot easily from one node to the next in describing a security event. As entities are populated in their respective vertex, an analyst will be able to read across the model to describe the specific activity (for example, FIN7 [the Adversary] uses phish kits [the Capability] and actor-registered domains [the Infrastructure] to target bank executives [the Victim]).<a href="#r1">(Chapman & Maymí)</a>

> The main axiom of this model states, “For every intrusion event there exists an adversary taking a step towards an intended goal by using a capability over infrastructure against a victim to produce a result.”<a href="#r3">(McMillan, Troy)</a>

> Finalized in 2013, the Diamond Model of Intrusion Analysis serves as a practical analytical methodology for cybersecurity analysts to utilize before, during, and after cybersecurity intrusions. <a href="#r4">(Rogers, Bobby)</a>

> The Diamond Model uses a number of specific terms: Core Features of an event, which are the adversary, capability, infrastructure, and victim (the vertices of the diamond). The Meta-Features, which are start and end timestamps, phase, result, direction, methodology, and resources. These are used to order events in a sequence known as an activity thread, as well as for grouping events based on their features. A Confidence Value, which is undefined by the model, but which analysts are expected to determine based on their own work.<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Kill chain</summary>
* [https://www.lockheedmartin.com/en-us/capabilities/cyber/cyber-kill-chain.html](https://www.lockheedmartin.com/en-us/capabilities/cyber/cyber-kill-chain.html)

> Conventional network defense tools such as intrusion detection systems and anti-virus focus on
the vulnerability component of risk, and traditional incident response methodology presupposes a
successful intrusion. An evolution in the goals and sophistication of computer network intrusions
has rendered these approaches insufficient for certain actors. A new class of threats, appropriately
dubbed the `Advanced Persistent Threat (APT)`, represents well-resourced and trained adversaries
that conduct multi-year intrusion campaigns targeting highly sensitive economic, proprietary, or
national security information. These adversaries accomplish their goals using advanced `tools and
techniques` designed to defeat most conventional computer network defense mechanisms. Network
defense techniques which leverage knowledge about these adversaries can create an `intelligence
feedback loop`, enabling defenders to establish a state of information superiority which decreases the
adversary’s likelihood of success with each subsequent intrusion attempt. Using a kill chain model to
describe phases of intrusions, mapping adversary kill chain indicators to defender courses of action,
identifying patterns that link individual intrusions into broader campaigns, and understanding the
iterative nature of intelligence gathering form the basis of intelligence-driven computer network defense
(CND). Institutionalization of this approach reduces the likelihood of adversary success, informs
network defense investment and resource prioritization, and yields relevant metrics of performance
and effectiveness. The evolution of advanced persistent threats necessitates an intelligence-based
model because in this model the defenders mitigate not just vulnerability, but the threat component
of risk, too <a href="#r9">Lockheed Martin Corp</a>.

1. `Reconnaissance` - Research, identification and selection of targets, often represented as crawling
Internet websites such as conference proceedings and mailing lists for email addresses, social
relationships, or information on specific technologies.
2. `Weaponization` - Coupling a remote access trojan with an exploit into a deliverable payload,
typically by means of an automated tool (weaponizer). Increasingly, client application data files such
as Adobe Portable Document Format (PDF) or Microsoft Office documents serve as the weaponized
deliverable.
3. `Delivery` - Transmission of the weapon to the targeted environment. The three most prevalent
delivery vectors for weaponized payloads by APT actors, as observed by the Lockheed Martin
Computer Incident Response Team (LM-CIRT) for the years 2004-2010, are email attachments,
websites, and USB removable media.
4. `Exploitation` - After the weapon is delivered to victim host, exploitation triggers intruders’ code.
Most often, exploitation targets an application or operating system vulnerability, but it could also
more simply exploit the users themselves or leverage an operating system feature that auto-executes
code
5. `Installation` - Installation of a remote access trojan or backdoor on the victim system allows the
adversary to maintain persistence inside the environment.
6. `Command and Control (C2)` - Typically, compromised hosts must beacon outbound to an
Internet controller server to establish a C2 channel. APT malware especially requires manual
interaction rather than conduct activity automatically. Once the C2 channel establishes, intruders
have “hands on the keyboard” access inside the target environment.
7. `Actions on Objectives` - Only now, after progressing through the first six phases, can intruders
take actions to achieve their original objectives. Typically, this objective is data exfiltration which
involves collecting, encrypting and extracting information from the victim environment; violations
of data integrity or availability are potential objectives as well. Alternatively, the intruders may
only desire access to the initial victim box for use as a hop point to compromise additional systems
and move laterally inside the network <a href="#r9">Lockheed Martin Corp</a>..


```mermaid
flowchart LR
   Reconnaissance --> Weaponization --> Delivery --> Exploitation --> Installation --> C2 --> Actions
```

>  if defenders can stop an attack early on at the exploitation stage, they can have confidence that the attacker is far less likely to have progressed to further stages. Defenders can therefore avoid conducting a full incident response plan. Furthermore, understanding the phase progression, typical behavior expected at each phase, and inherent dependencies in the overall process allows for defenders to take appropriate measures to disrupt the kill chain.<a href="#r1">(Chapman & Maymí)</a>

> The cyber kill chain is a cyber intrusion identification and prevention model developed by Lockheed Martin that describes the stages of an intrusion.<a href="#r3">(McMillan, Troy)</a>

> The Cyber Kill Chain is sometimes criticized for emphasizing perimeter security countermeasures at the expense of internal security. Accordingly, a stronger Unified Kill Chain—which is an extended hybrid of the Cyber Kill Chain and MITRE’s ATT&CK framework—was created to enhance and balance the perimeter/internal security zones.<a href="#r4">(Rogers, Bobby)</a>

</details>

### Threat research

Regardless of the type of threat intelligence you acquire through research, you can categorize the information in three general ways <a href="#r4">(Rogers, Bobby)</a>:
- Strategic: Intelligence that identifies the long-term and “big picture” viewpoint about adversaries and threat trends, likely targets, plus adversarial motivations.
- Operational: Intelligence that identifies threat methodologies, attacker tools of the trade, and tactics, techniques, and procedures.
- Tactical: Intelligence that leads to the identification of current or imminent IOCs, including malicious domain names, URLs, e-mail addresses, IP addresses, and hash values.

<details>
  <summary markdown="span">Reputational</summary>

> Cisco’s threat intelligence team, Talos, provides excellent reputational lookup features in a single dashboard in its Reputation Center service. Figure 2-5 is a snapshot of the report page generated for a suspicious `IP address`. Included in the report are details about `location`, `blacklist status`, and `IP address owner `information. For the Reputation Details section, Talos breaks the reputational assessments out by e-mail, malware, and spam, with historical information to give a sense of trends associated with the IP address. Finally, at the bottom of the report is a section for additional information. In this case, the IP address carried “critical” and “very high” spam levels from the previous day and month, respectively. <a href="#r1">(Chapman & Maymí)</a>

> As malicious traffic is received by customers, reputational scores are developed for `IP ranges`, `domain names`, and `URLs` that serve as sources of the traffic. Based on these `scores`, traffic may be blocked from those sources on the customer networks.<a href="#r3">(McMillan, Troy)</a>

>  Reputation data tends to describe suspicious `DNS names`, `e-mail addresses`, `file hashes`, `IP addresses`, `URLs`, and `websites`. Then, because it’s now easier for us to determine “friend” or “foe,” threats are formally assigned `reputational scores`. Higher scores indicate generally positive reputations, whereas `lower scores indicate generally negative reputations`. This information can then be automatically or manually distributed globally as part of threat intelligence sharing platforms.<a href="#r4">(Rogers, Bobby)</a>

> Although the CySA+ exam lists “reputational” as a type of threat research, you're more likely to run into the term in use when describing the impact or damage that a threat can cause. `Reputational damage`, sometimes called brand damage, is a major concern for many organizations. Thus, if you search for “threat” and “reputational” you won't find a lot about reputational threat research, but you will find a lot about `reputational damage`.<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Behavioral</summary>

  * [https://openuba.org/](https://openuba.org/)

> A [heuristic](https://en.wikipedia.org/wiki/Heuristic) (/hjʊˈrɪs.tɪk/; from Ancient Greek εὑρίσκω (heurískō) 'I find, discover'), or heuristic technique, is any approach to problem solving or self-discovery that employs a practical method that is not guaranteed to be optimal, perfect, or rational, but is nevertheless sufficient for reaching an immediate, short-term goal or approximation.

  > [Cuckoo Sandbox](https://cuckoo.sh/docs/) is an open source software for automating analysis of suspicious files. To do so it makes use of custom components that monitor the behavior of the malicious processes while running in an isolated environment.

> Sometimes we are unable or unwilling to invest the effort into `reverse engineering` a binary executable, but we still want to find out what it does. This is where an isolation environment, or sandbox, comes in handy. Unlike endpoint protection sandboxes, this tool is usually instrumented to assist the security analyst in understanding what a running executable is doing as samples of malware are executed to determine their behaviors <a href="#r1">(Chapman & Maymí)</a>.

> Behavioral analysis is also known as anomaly analysis, because it also observes network behaviors for anomalies. It can be implemented using combinations of the scanning types, including NetFlow, protocol, and packet analyses, to create a baseline and subsequently report departures from the traffic metrics found in the baseline. One of the newer advances in this field is the development of `user and entity behavior analytics (UEBA)`. This type of analysis focuses on user activities. Combining behavior analysis with machine learning, UEBA enhances the ability to determine which particular users are behaving oddly. An example would be a hacker who has stolen credentials of a user and is identified by the system because he is not performing the same activities that the user would perform <a href="#r3">(McMillan, Troy)</a>.

> Behavioral threat detection involves first understanding how our environment normally behaves over a period of time and then `identifying patterns of behavior` that deviate from the norm <a href="#r4">(Rogers, Bobby)</a>.

> Behavioral assessments are particularly useful for insider threats because insider threat behavior is often difficult to distinguish from job- or role-related work <a href="#r5">(Chapple & Seidl)</a>.

</details>


<details>
  <summary markdown="span">Indicator of compromise (IoC)</summary>

  * [https://github.com/RedDrip7/APT_Digital_Weapon](https://github.com/RedDrip7/APT_Digital_Weapon)
  * [https://github.com/eset/malware-ioc](https://github.com/eset/malware-ioc)
  * [https://snort.org/](https://snort.org/)
  * [https://virustotal.github.io/yara/](https://virustotal.github.io/yara/)

  ##### The Pyramid of Pain
The below is the IoC [Pyramid of Pain](http://detect-respond.blogspot.com/2013/03/the-pyramid-of-pain.html) authored by Blogger David J Bianco on the 1st of March 2013. 

<img src="https://4.bp.blogspot.com/-EDLbyYipz_E/UtnWN7fdGcI/AAAAAAAANno/b4UX5wjNdh0/s1600/Pyramid+of+Pain+v2.png" alt="pyramind_of_pain">

1. `Hash Values`: SHA1, MD5 or other similar hashes that correspond to specific suspicious or malicious files.  Often used to provide unique references to specific samples of malware or to files involved in an intrusion.
2. `IP Addresses`:  It's, um, an IP address.  Or maybe a netblock.
3. `Domain Names`: This could be either a domain name itself (e.g., "evil.net") or maybe even a sub- or sub-sub-domain (e.g., "this.is.sooooo.evil.net")
4. `Network Artifacts`: Observables caused by adversary activities on your network. Technically speaking, every byte that flows over your network as a result of the adversary's interaction could be an artifact, but in practice this really means those pieces of the activity that might tend to distinguish malicious activity from that of legitimate users.  Typical examples might be URI patterns, C2 information embedded in network protocols, distinctive HTTP User-Agent or SMTP Mailer values, etc.
5. `Host Artifacts`: Observables caused by adversary activities on one or more of your hosts.  Again, we focus on things that would tend to distinguish malicious activities from legitimate ones.  They could be registry keys or values known to be created by specific pieces of malware, files or directories dropped in certain places or using certain names, names or descriptions or malicious services or almost anything else that's distinctive.
6. `Tools`: Software used by the adversary to accomplish their mission.  Mostly this will be things they bring with them, rather than software or commands that may already be installed on the computer.  This would include utilities designed to create malicious documents for spearphishing, backdoors used to establish C2 or password crackers or other host-based utilities they may want to use post-compromise.
7. `Tactics, Techniques and Procedures (TTPs)`: How the adversary goes about accomplishing their mission, from reconnaissance all the way through data exfiltration and at every step in between.  "Spearphishing" is a common TTP for establishing a presence in the network.  "Spearphishing with a trojaned PDF file" or "... with a link to a malicious .SCR file disguised as a ZIP" would be more specific versions.  "Dumping cached authentication credentials and reusing them in Pass-the-Hash attacks" would be a TTP.  Notice we're not talking about specific tools here, as there are any number of ways of weaponizing a PDF or implementing Pass-the-Hash.


  ##### Example Snort Rule

```bash
   alert tcp any any <> any 80 (msg:"SHA256 Alert"; 
    protected_content:"56D6F32151AD8474F40D7B939C2161EE2BBF10023F4AF1DBB3E13260EBDC6342"; 
    hash:sha256; offset:0; length:4;)
```

##### Example Yara Rule

```json
rule ExampleRule
{
    strings:
        $my_text_string = "text here"
        $my_hex_string = { E2 34 A1 C8 23 FB }

    condition:
        $my_text_string or $my_hex_string
}
```

> IOCs need two primary components: data and context <a href="#r1">(Chapman & Maymí)</a>.

> An indicator of compromise (IoC) is any activity, artifact, or log entry that is typically associated with an attack of some sort. Typical examples include the following <a href="#r3">(McMillan, Troy)</a>:
- Virus signatures
- Known malicious file types
- Domain names of known botnet servers

> As IOCs become known to your organization or others, they can be centrally shared with intelligence communities or shared peer-to-peer with organizations to enhance our global threat intelligence capabilities.<a href="#r4">(Rogers, Bobby)</a>

> Knowing which IOCs are associated with a given threat actor, or common exploit path, can help defenders take appropriate steps to prevent further compromise and possibly to identify the threat actor. It can also help defenders limit the damage or stop the attack from progressing.<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Common vulnerability scoring system (CVSS)</summary>

>  CVSS consists of three metric groups: `Base`, `Temporal`, and `Environmental`. The `Base` group represents the intrinsic qualities of a vulnerability that are `constant over time` and across user environments, the `Temporal` group reflects the characteristics of a vulnerability that `change over time`, and the `Environmental` group represents the characteristics of a vulnerability that are `unique` to a user's environment. The Base metrics produce a score ranging from `0 to 10`, which can then be modified by scoring the Temporal and Environmental metrics. A CVSS score is also represented as a vector string, a compressed textual representation of the values used to derive the score. This document provides the official specification for CVSS version 3.1.

<img src="images/cvss3.png" alt="cvss3.png">

> The [Temporal metric group](https://www.first.org/cvss/v3.1/specification-document) reflects the characteristics of a vulnerability that may `change over time but not across user environments`. For example, the presence of a simple-to-use exploit kit would increase the CVSS score, while the creation of an official patch would decrease it.

<img src="images/cvss4.png" alt="cvss4.png">

> The [Environmental metric group](https://www.first.org/cvss/v3.1/specification-document) represents the characteristics of a vulnerability that are `relevant` and `unique` to a particular user’s `environment`. Considerations include the presence of security controls which may mitigate some or all consequences of a successful attack, and the relative importance of a vulnerable system within a technology infrastructure.

<img src="images/cvss5.png" alt="cvss5.png">

> The [Base Score](https://www.first.org/cvss/v3.1/specification-document) can then be refined by scoring the Temporal and Environmental metrics in order to more accurately reflect the relative severity posed by a vulnerability to a user’s environment at a specific point in time. Scoring the Temporal and Environmental metrics is `not required`, but is recommended for `more precise scores`.

<img src="images/cvss2.png" alt="cvss2.png">

##### Example CVE to CVSS
* [https://nvd.nist.gov/vuln/detail/CVE-2021-38198](https://nvd.nist.gov/vuln/detail/CVE-2021-38198)


CVE-2021-38198 was released on 08AUG21 and was most recently modified on 15OCT21. The Analyst description is as follows:

> arch/x86/kvm/mmu/paging_tmpl.h in the Linux kernel before 5.12.11 incorrectly computes the access permissions of a shadow page, leading to a missing guest protection page fault. NIST gives this CVE a Base Score of `5.5 medium`. With a Vector of: ` CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`. Using the [Common Vulnerability Scoring System Version 3.1 (CVSS v3.1) Calculator](https://www.first.org/cvss/calculator/3.1) we can see the following from this score:

<img src="images/cvss1.png" alt="cvss1.png">

See other examples on the first.org website here:
* [https://www.first.org/cvss/v3.1/examples](https://www.first.org/cvss/v3.1/examples)
* [https://docs.hackerone.com/hackers/severity.html](https://docs.hackerone.com/hackers/severity.html)
* [https://hackerone.com/gsa_bbp?type=team](https://hackerone.com/gsa_bbp?type=team)

> The Common Vulnerability Scoring System is the de facto standard for assessing the severity of vulnerabilities. Therefore, you should be familiar with CVSS and its metric groups: base, temporal, and environmental. These groups represent various aspects of a vulnerability. Base are those characteristics that do not change over time, temporal describes those that do, and environmental represents those that are unique to a user’s environment.<a href="#r1">(Chapman & Maymí)</a>

Hackerone now uses [GSA Bounty](https://hackerone.com/gsa_bbp?type=team) for their Bug Bounty program. The policy used is stated below:

> The bug bounty program of the General Services Administration is special in that it aims to cover numerous individual services that have been developed to address a diverse range of public use cases. Our strategy is to rotate services into scope at regular intervals. Our rewards are based on severity per CVSS (the Common Vulnerability Scoring Standard). Please note these are general guidelines, and reward decisions are up to the discretion of GSA.

> The Common Vulnerability Scoring System (CVSS) provides a way to capture the principal characteristics of a vulnerability and produce a numerical score reflecting its severity. The numerical score can then be translated into a qualitative representation (such as low, medium, high, and critical) to help organizations properly assess and prioritize their vulnerability management processes. CVSS is a published standard used by organizations worldwide, and the SIG's mission is to continue to improve it.<a href="#r2">(Firts.org)</a>

##### CVSS VECTORS
A vulnerability with Base metric values of “Attack Vector: Network, Attack Complexity: Low, Privileges Required: High, User Interaction: None, Scope: Unchanged, Confidentiality: Low, Integrity: Low, Availability: None” and no specified Temporal or Environmental metrics would produce the following vector:

- [CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:L/A:N](https://www.first.org/cvss/v3.1/specification-document)

The same example with the addition of “Exploitability: Functional, Remediation Level: Not Defined” and with the metrics in a non-preferred ordering would produce the following vector:

- [CVSS:3.1/S:U/AV:N/AC:L/PR:H/UI:N/C:L/I:L/A:N/E:F/RL:X](https://www.first.org/cvss/v3.1/specification-document)





</details>

### Threat modeling methodologies
> To gain the greatest benefit from threat modeling, it should be performed early and continuously as an input directly into the software development lifecycle (SDLC).<a href="#r1">(Chapman & Maymí)</a>

> An organization should have a well-defined risk management process in place that includes the evaluation of risk that is present. When this process is carried out properly, a threat modeling methodology allows organizations to identify threats and potential attacks and implement the appropriate mitigations against these threats and attacks. <a href="#r3">(McMillan, Troy)</a>

> In its most common usage, threat modeling is the practice of identifying, prioritizing, and mitigating threats across all phases of a system’s lifecycle. Unlike more reactive security practices, threat modeling seeks to address potential threat events early in the process before they become realized.<a href="#r4">(Rogers, Bobby)</a>


<details>
  <summary markdown="span">Adversary capability</summary>

> [MITRE ATT&CK ID: T1587](https://attack.mitre.org/techniques/T1587/) Develop Capabilities: Adversaries may build capabilities that can be used during targeting. Rather than purchasing, freely downloading, or stealing capabilities, adversaries may develop their own capabilities in-house. This is the process of identifying development requirements and building solutions such as malware, exploits, and self-signed certificates. Adversaries may develop capabilities to support their operations throughout numerous phases of the adversary lifecycle.

> Understanding what a potential attacker is capable of can be a daunting task, but it is often a key competence of any good threat intelligence team. The first step in understanding adversary capability is to document the types of threat actors that would likely be threats, what their intent might be, and what capabilities they might bring to bear in the event of a security incident. As previously described, we can develop an understanding of adversary TTPs with the help of various attack frameworks and resources such as MITRE ATT&CK <a href="#r1">(Chapman & Maymí)</a>.

> Security professionals should analyze all the threats to identify all the actors who pose significant threats to the organization. Examples of the threat actors include both internal and external actors<a href="#r3">(McMillan, Troy)</a>

> Our adversaries’ capabilities are characterized in terms of their resources, methods, and attack vectors <a href="#r4">(Rogers, Bobby)</a>.

•   `Resources`   How much expertise do they have? How well funded are they? What technical resources can they employ? Adversarial resources can range from severely limited to national-level sophistication and strength.

•   `Methods`   Are their methods simplistic or very sophisticated? Will our adversaries use someone else’s tools and malware, or will they develop their own specifically designed to attack us?

•   `Attack vectors`  Will they use cyber-based attacks, human-based attacks, or both? Will they attack us directly or go after our supply chain, which can include our vendors, suppliers, partners, ISP, and customers? Will they exploit our physical security? Wi-Fi? E-mail?


</details>


<details>
  <summary markdown="span">Total attack surface</summary>

* [https://medium.com/mitre-attack/the-retirement-of-pre-attack-4b73ffecd3d3](https://medium.com/mitre-attack/the-retirement-of-pre-attack-4b73ffecd3d3)

[MITRE ATT&CK ID: TA0001](https://attack.mitre.org/tactics/TA0001/) Initial Access consists of techniques that use various entry vectors to gain their initial foothold within a network. Techniques used to gain a foothold include targeted spearphishing and exploiting weaknesses on public-facing web servers. Footholds gained through initial access may allow for continued access, like valid accounts and use of external remote services, or may be limited-use due to changing passwords.

[MITRE ATT&CK PRE Matrix](https://attack.mitre.org/matrices/enterprise/pre/): Tactics and techniques representing the MITRE ATT&CK® Matrix for Enterprise covering preparatory techniques. The Matrix contains information for the PRE platform. 

<img title="pre-matrix" alt="Alt text" src="/images/pre.png">

> The attack surface is the logical and physical space that can be targeted by an attacker. Logical areas include infrastructure and services, while physical areas include server rooms and workstations <a href="#r1">(Chapman & Maymí)</a>.

> To determine the security controls that can be used, the organization would need to look at industry standards, including NIST SP 800-53 (revision 4 at the time of writing). Finally, the organization would map controls back into the attack tree to ensure that controls are implemented at as many levels of the attack surface as possible <a href="#r3">(McMillan, Troy)</a>.

> Total attack surfaces are the sum of all areas of our network, systems, or software that contain vulnerabilities accessible to threat actors for exploitation <a href="#r4">(Rogers, Bobby)</a>.


</details>


<details>
  <summary markdown="span">Attack vector</summary>

The below is from CVSS v3.1 Base Metric value [Attack Vector (AV)](https://www.first.org/cvss/v3.1/specification-document):

> This metric reflects the context by which vulnerability exploitation is possible. This metric value (and consequently the Base Score) will be larger the more remote (logically, and physically) an attacker can be in order to exploit the vulnerable component. The assumption is that the number of potential attackers for a vulnerability that could be exploited from across a network is larger than the number of potential attackers that could exploit a vulnerability requiring physical access to a device, and therefore warrants a greater Base Score. The list of possible values is presented in Table 1 <a href="#r2">(Firts.org)</a>.

<img title="cvss-vector" alt="Alt text" src="/images/cvss-vector.png">

> With a potential adversary in mind and critical assets identified, the natural next step is to determine the most likely path for the adversary to get their hands on the goods. This can be done using visual tools, as part of a red-teaming exercise, or even as a tabletop exercise. The goals of mapping out attack vectors consist of identifying realistic or likely paths to critical assets and identifying which security controls are in place to mitigate specific TTPs <a href="#r1">(Chapman & Maymí)</a>.

> Once attack vectors and attack agents have been identified, the organization must assess the relative impact and likelihood of such attacks. This allows the organization to prioritize the limited resources available to address the vulnerabilities <a href="#r3">(McMillan, Troy)</a>.

> If total attack surfaces are the sum, attack vectors make up its parts. Attack vectors are individual pathways or methods by which threat actors can gain unauthorized access to systems <a href="#r4">(Rogers, Bobby)</a>.


</details>


<details>
  <summary markdown="span">Impact</summary>

> [FIPS Publication 199](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.199.pdf) defines three levels of potential `impact` on organizations or individuals should there be a breach of security (i.e., a loss of confidentiality, integrity, or availability). The application of these definitions must take place within the context of each organization and the overall national interest. 
- `The potential impact is LOW if` The loss of confidentiality, integrity, or availability could be expected to have a limited adverse 
effect on organizational operations, organizational assets, or individuals.
- `The potential impact is MODERATE if` The loss of confidentiality, integrity, or availability could be expected to have a serious adverse effect on organizational operations, organizational assets, or individuals. 
- `The potential impact is HIGH if` The loss of confidentiality, integrity, or availability could be expected to have a severe or 
catastrophic adverse effect on organizational operations, organizational assets, or individuals.

<img title="fips199" alt="Alt text" src="/images/fips199.png">

> The CVSS v3.1 Base metric group represents the intrinsic characteristics of a vulnerability that are constant over time and across user environments. It is composed of two sets of metrics: the `Exploitability metrics` and the [Impact metrics](https://www.first.org/cvss/v3.1/specification-document).

<img title="impact" alt="Alt text" src="/images/impact.png">

<img title="impact" alt="Alt text" src="/images/impact2.png">


[MITRE ATT&CK ID: TA0040](https://attack.mitre.org/tactics/TA0040/) Impact consists of techniques that adversaries use to disrupt availability or compromise integrity by manipulating business and operational processes. Techniques used for impact can include destroying or tampering with data. In some cases, business processes can look fine, but may have been altered to benefit the adversaries’ goals. These techniques might be used by adversaries to follow through on their end goal or to provide cover for a confidentiality breach.


> Impact types can include but aren’t limited to physical, logical, monetary, and reputational <a href="#r1">(Chapman & Maymí)</a>.

> Typically a risk assessment matrix is created <a href="#r3">(McMillan, Troy)</a>.

<img title="matrix" alt="Alt text" src="/images/matrix.png">


> Here are some examples of organizational impacts <a href="#r4">(Rogers, Bobby)</a>:

•   Breaches of legal, regulatory, or contractual requirements

•   Classification level of impacted information (Confidential, Secret, Top Secret)

•   Confidentiality, integrity, and availability requirements of breached assets

•   Damage of organizational reputation

•   Disruption of organizational plans and deadlines

•   Loss of business and financial value.


</details>


<details>
  <summary markdown="span">Likelihood</summary>

<a href="https://csrc.nist.gov/glossary/term/likelihood">
<img title="like" alt="Alt text" src="/images/like.png">
</a>

CVSS v3.1 Exploit Code Maturity (E) metric measures likelihood:

<a href="https://www.first.org/cvss/v3.1/specification-document">
<img title="cvss-like" alt="Alt text" src="/images/cvss-like.png">
</a>

Potential impacts must be balanced by the likelihood of occurring in the first place. Although asteroids careening into our building would generate severe impact, that’s very unlikely to happen. Malicious port scanning is highly likely to occur, yet its immediate impact is minor or negligible at best. Impact or likelihood by themselves don’t mean much, but, taken together, they help clarify the degree of risk we should ascribe to threats <a href="#r4">(Rogers, Bobby)</a>.

</details>

### Threat intelligence sharing with supported functions

<details>
  <summary markdown="span">Incident response</summary>

* [https://www.cisa.gov/incident-response-training](https://www.cisa.gov/incident-response-training)

<a href="https://csrc.nist.gov/glossary/term/incident_response">
<img title="like" alt="incident responce nist" src="/images/ir.png">
</a>

[IBM](https://www.ibm.com/topics/incident-response) defines Incident Response as: 

> Incident response is an organization's systematic reaction to an information security breach attempt.

> Incident response is not usually an entry-level security function because it requires such a diverse skill set, from malware analysis, to forensics, to network traffic analysis <a href="#r1">(Chapman & Maymí)</a>.


> Incident response focuses on detecting and responding to cybersecurity incidents. Its reliance on cutting-edge threat data makes it a perfect candidate for the information received through threat intelligence sharing mechanisms. As updated threat data is fed into an organization’s security information event management (SIEM) tool, such as Splunk Enterprise or LogRhythm, the organization can accelerate its incident response and recovery actions much earlier into an adversary’s attack cycle.<a href="#r4">(Rogers, Bobby)</a>.


</details>


<details>
  <summary markdown="span">Vulnerability management</summary>

<a href="https://csrc.nist.gov/glossary/term/iscm">
<img title="ISCM" alt="ISCM nist" src="/images/iscm.png">
</a>


<a href="https://csrc.nist.gov/glossary/term/iscm_capability">
<img title="ISCM Capability" alt="ISCM Capability nist" src="/images/iscmc.png">
</a>

<a href="https://csrc.nist.gov/glossary/term/vulnerability_management">
<img title="vulnerability management" alt="vulnerability management nist" src="/images/vulman.png">
</a>

> there is no function that depends so heavily on shared intelligence information as vulnerability management. When sharing platforms and protocols are used to identify new threats, this data must be shared in a timely manner with those managing vulnerabilities <a href="#r3">(McMillan, Troy)</a>.

> Vulnerability management is the ongoing process of identifying, classifying, prioritizing, and remediating software vulnerabilities. Threat intelligence sharing is crucial to this effort through the acquisition of timely information on real-time threats, which helps improve detection and mitigation response times <a href="#r4">(Rogers, Bobby)</a>.
</details>


<details>
  <summary markdown="span">Risk management</summary>

<a href="https://csrc.nist.gov/glossary/term/risk_management">
<img title="risk management" alt="risk management nist" src="/images/riskmanagement.png">
</a>


<a href="https://csrc.nist.gov/projects/risk-management">
<img title="risk management framework" alt="risk management framework nist" src="/images/nistrm.png">
</a>


<a href="https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.200.pdf">
<img title="FIPS200" alt="FIPS200" src="/images/FIPS200.png">
</a>

> risk management teams speaking the language of risk in terms of impact and probability. If we understand risk to mean the impact to an asset by a threat actor exploiting a vulnerability, we see that the presence of a threat actor is necessary in communicating risk accurately. Drilling down further, three components need to be present for a threat to be accurately described: capability, intent, and opportunity <a href="#r1">(Chapman & Maymí)</a>.

> It is a formal process that rates identified vulnerabilities by the likelihood of their compromise and the impact of said compromise <a href="#r3">(McMillan, Troy)</a>.


</details>


<details>
  <summary markdown="span">Security engineering</summary>


<a href="https://csrc.nist.gov/glossary/term/security_engineering">
<img title="security engineering nist" alt="security engineering nist" src="/images/seceng.png">
</a>

> Security engineering is the process of architecting security features into the design of a system or set of systems. It has as its goal an emphasis on security from the ground up, sometimes stated as “building in security.” Unless the very latest threats are shared with this function, engineers cannot be expected to build in features that prevent threats from being realized<a href="#r3">(McMillan, Troy)</a>.

> Whereas cybersecurity analysis focuses more on the “offensive” side of security—identifying security issues, threat hunting, performing vulnerability assessments, and penetration tests—security engineering emphasizes the “defensive” aspects by constructing security solutions. Informed through the exchange of real-time threat information, organizations will respond by either enriching their existing security controls or implementing more “targeted defenses,”<a href="#r4">(Rogers, Bobby)</a>

</details>


<details>
  <summary markdown="span">Detection and monitoring</summary>

> For analysts working in a security operations center (SOC) to interpret incoming detection alerts, context is critical in enabling them to triage quickly and move on to scoping potential incidents<a href="#r1">(Chapman & Maymí)</a>.

> Finally, those who are responsible for monitoring and detecting attacks also benefit greatly from timely sharing of threat intelligence data. Without this, indicators of compromise cannot be developed and utilized to identify the new threats in time to stop them from causing breaches<a href="#r3">(McMillan, Troy)</a>.

> Detection can happen in many ways. An end user could notice some strange occurrences on their computer, such as sluggishness or applications doing “funny things.” A system administrator could see some curious patterns in log files, or a cybersecurity technician could be alerted through an intrusion detection or prevention system (IDS/IPS) or a security information and event management (SIEM) system. No matter how the incident is detected, as soon as it happens, several things must occur very quickly. First, the frontline responder, typically a cybersecurity analyst, must determine whether or not this is a true incident. If they have insufficient information to make this determination, they should elevate the issue to management or even the incident response team. The incident response team, if provided with enough details, can determine as to whether this is an incident that requires a response or if it’s something that a technician can simply resolve at their level<a href="#r4">(Rogers, Bobby)</a>.
</details>

## 1.3 Given a scenario, perform vulnerability management activities.
### Vulnerability identification

> Vulnerability identification: Explores asset criticality, active vs. passive scanning, and mapping/enumeration<a href="#r3">(McMillan, Troy)</a>.

<details>
  <summary markdown="span">Asset criticality</summary>

> Asset classification is critical to all systems to protect the confidentiality, integrity, and availability (CIA) of the asset. After assets are classified, they can be segmented based on the level of protection needed. The classification levels ensure that assets are protected in the most cost-effective manner possible <a href="#r3">(McMillan, Troy)</a>.

> Remember, the identification of critical assets helps anchor the organization’s vulnerability remediation efforts to the most critical assets first <a href="#r4">(Rogers, Bobby)</a>.

</details>


<details>
  <summary markdown="span">Active vs. passive scanning</summary>

<a href="#r3">(McMillan, Troy)</a>
> Active scanning   Occurs when a scanning tool identifies system vulnerabilities by communicating directly with the system. For example, the OpenVAS vulnerability scanner scans a system and identifies dozens of well-known Windows 10 vulnerabilities officially tagged with CVE IDs <a href="#r4">(Rogers, Bobby)</a>.

> Passive scanning   Occurs when a scanning tool indirectly identifies system vulnerabilities but does not communicate with the system. Instead, the scanner looks for any unusual behaviors or known issues by observing the system’s network traffic. As an example, the Wireshark packet sniffer captures packets coming from an Internet-facing web server sending out NetBIOS or SMB traffic—which will invite all kinds of trouble if not immediately resolved <a href="#r4">(Rogers, Bobby)</a>.

> Active scanning interacts with a host, whereas passive information gathering simply observes activity and draws conclusions <a href="#r5">(Chapple & Seidl)</a>.

</details>


<details>
  <summary markdown="span">Mapping/enumeration</summary>

<a href="https://csrc.nist.gov/Projects/Security-Content-Automation-Protocol/Specifications/cpe">
<img title="cpe" alt="cpe" src="/images/cpe.png">
</a>

The nmap manpage states the following about its use of the [Common Platform Enumeration (CPE)](https://csrc.nist.gov/Projects/Security-Content-Automation-Protocol/Specifications/cpe).

> After TCP and/or UDP ports are discovered using one of the other scan methods, version detection interrogates those ports to determine more about what is actually running. The nmap-service-probes database contains probes for querying various services and match expressions to recognize and parse responses. Nmap tries to determine the service protocol (e.g. FTP, SSH, Telnet, HTTP), the application name (e.g. ISC BIND, Apache httpd, Solaris telnetd), the version number, hostname, device type (e.g. printer, router), the OS family (e.g. Windows, Linux). When possible, Nmap also gets the Common Platform Enumeration (CPE) representation of this information.


> The goal of network mapping is to understand the topology of the network, including perimeter networks, demilitarized zones, and key network devices. The process used during network mapping is referred to as topology discovery <a href="#r1">(Chapman & Maymí)</a>.

> A closely related concept is the [Common Weakness Enumeration (CWE)](https://cwe.mitre.org/), a category system for software weaknesses and vulnerabilities. CWE organizes vulnerabilities into over 600 categories, including classes for buffer overflows, path/directory tree traversal errors, race conditions, cross-site scripting, hard-coded passwords, and insecure random numbers <a href="#r3">(McMillan, Troy)</a>.

> Sometimes referred to as vulnerability mapping or enumeration, vulnerability assessments are the broader process of identifying, quantifying, and prioritizing vulnerabilities with our devices and software <a href="#r4">(Rogers, Bobby)</a>.

> Standards for penetration testing typically include enumeration and reconnaissance processes and guidelines. There are a number of publicly available resources, including the (Open Source Security Testing Methodology Manual (OSSTMM)](https://www.isecom.org/OSSTMM.3.pdf), the [Penetration Testing Execution Standard](http://www.pentest-standard.org/index.php/Main_Page), and [National Institute of Standards and Technology (NIST) Special Publication 800-115](https://csrc.nist.gov/publications/detail/sp/800-115/final), the Technical Guide to Information Security Testing and Assessment <a href="#r5">(Chapple & Seidl)</a>.


</details>

### Validation
As a general rule False is BAD and True is GOOD.

The following example is from Google's Machine Learning Crash Course lesson [Classification: True vs. False and Positive vs. Negative](https://developers.google.com/machine-learning/crash-course/classification/true-false-positive-negative)

<a href="https://developers.google.com/machine-learning/crash-course/classification/true-false-positive-negative">
<img title="true false positive negative" alt="true false positive negative" src="/images/googlewolf.png">
</a>

<details>
  <summary markdown="span">True positive</summary>


> Occurs when the scanner correctly identifies a vulnerability <a href="#r3">(McMillan, Troy)</a>.

> A true positive occurs when an assessment tool correctly indicates the presence of a specific vulnerability that we do in fact have. In other words, our tool cried “wolf” when there was indeed a wolf present <a href="#r4">(Rogers, Bobby)</a>.

> Tenable’s Nessus vulnerability scanner correctly detected a “critical” Linux vulnerability with Bash Remote Code Execution (Shellshock). The CVE ID is CVE-2014-6271 <a href="#r4">(Rogers, Bobby)</a>.

</details>


<details>
  <summary markdown="span">False positive</summary>

> Occurs when the scanner identifies a vulnerability that does not exist <a href="#r3">(McMillan, Troy)</a>.

> When a scanner reports a vulnerability that does not exist, this is known as a false positive error <a href="#r5">(Chapple & Seidl)</a>.

> A web application scanner indicates the presence of a specific SQL injection vulnerability when in reality there it isn’t present <a href="#r4">(Rogers, Bobby)</a>.

</details>


<details>
  <summary markdown="span">True negative</summary>

> Occurs when the scanner correctly determines that a vulnerability does not exist <a href="#r3">(McMillan, Troy)</a>.

> You use an Nmap script to scan a system for the Heartbleed bug but were unable to locate the vulnerability because the system already had implemented Heartbleed bug patches <a href="#r4">(Rogers, Bobby)</a>.

</details>


<details>
  <summary markdown="span">False negative</summary>

> Occurs when the scanner does not identify a vulnerability that exists <a href="#r3">(McMillan, Troy)</a>.

> Microsoft recently publicized new vulnerabilities with Windows 10. Using Nessus, you scan systems and were unable to detect the vulnerabilities. You then realize that Nessus hasn’t been updated in a week. After updating Nessus, you re-run the scans and successfully discover the recently publicized vulnerabilities. To be clear, this scenario has now changed from false negative to true positive <a href="#r4">(Rogers, Bobby)</a>.

</details>

### Remediation/mitigation

<details>
  <summary markdown="span">Configuration baseline</summary>


<a href="https://csrc.nist.gov/glossary/term/configuration_baseline">
<img title="configuration baseline" alt="configuration baseline" src="/images/configb.png">
</a>

> One practice that can make maintaining security simpler is to create and deploy standard images that have been secured with security baselines. A security baseline is a set of configuration settings that provide a floor of minimum security in the image being deployed <a href="#r3">(McMillan, Troy)</a>.

> What if a new configuration results in no change in vulnerability status, introduces a new vulnerability, or causes a system functionality loss? If remediation goes wrong, we can always return a system’s configuration to the most recent configuration baseline that worked <a href="#r4">(Rogers, Bobby)</a>.

> When you do perform mitigation activities, it's important to remember to update your configuration baseline as well. For example, if you apply a security patch to your systems, you should also modify your configuration baseline to ensure that future systems are patched against that same vulnerability from the start <a href="#r5">(Chapple & Seidl)</a>.

</details>


<details>
  <summary markdown="span">Patching</summary>


<a href="https://csrc.nist.gov/glossary/term/patch_management">
<img title="patch management" alt="patch management" src="/images/patch-management.png">
</a>

> Microsoft recently ended its practice of “Patch Tuesday,” however, in part due to this predictability. A downside of the practice emerged as attackers began reverse engineering the fixes as soon as they were released to determine the previously unknown vulnerabilities. Attackers knew that many administrators wouldn’t be able to patch all their machines before they figured out the vulnerability, and thus the moniker “Exploit Wednesday” emerged. Although darkly humorous, it was a major drawback that convinced the company to focus instead on improving its automatic update features <a href="#r1">(Chapman & Maymí)</a>.

> The patch management life cycle includes the following steps <a href="#r3">(McMillan, Troy)</a>:

- `Step 1`. Determine the priority of the patches and schedule the patches for deployment.

- `Step 2`. Test the patches prior to deployment to ensure that they work properly and do not cause system or security issues.

- `Step 3`. Install the patches in the live environment.

- `Step 4`. After the patches are deployed, ensure that they work properly.

> Patches come in all sorts of varieties; the following list details the types of patches you can see in a Microsoft environment. Although other operating systems (such as macOS and Linux) have different names for their updates, they are similar in functionality <a href="#r4">(Rogers, Bobby)</a>.

- `Security patch`   Software updates that fix operating system and application vulnerabilities. Typically scheduled for release on a predictable cycle (like Microsoft’s original Patch Tuesday).
- `Hotfix Critical updates` for various software issues that, unlike patches, should not be delayed.
- `Service packs`   Large collection of updates for a particular operating system or application released as one installable package. A service pack usually includes new features, patches, and hotfixes. This is now frowned upon due to the turbulence experienced by combining new features and patches.
- `Rollups`   Cumulative updates that contain a group of patches or hotfixes for a particular piece of software.
- `Feature updates (bi-annually)`   New versions of Windows 10 that come out roughly every six months during the spring and fall. They are focused on providing new OS features only.
- `Quality updates (monthly)`   Monthly cumulative updates to Windows 10 patches that fix bugs and errors, patch security vulnerabilities, and improve the reliability with the OS. They do not include new features.


</details>


<details>
  <summary markdown="span">Hardening</summary>


<a href="https://csrc.nist.gov/glossary/term/hardening">
<img title="hardening" alt="hardening" src="/images/hardening.png">
</a>

> The UDP and TCP ports between 0 and 1023 are known as the well-known ports because they are used for commonly used services. Some notable well-known ports are 20 (FTP), 22 (SSH), 25 (SMTP), and 80 (HTTP). Ports 1024 to 49151 are registered ports, and ports above 49151 are ephemeral or dynamic ports <a href="#r1">(Chapman & Maymí)</a>.

> The hardening can be accomplished both on physical and logical bases. From a logical perspective <a href="#r3">(McMillan, Troy)</a>:
- `Remove` unnecessary applications.
- `Disable` unnecessary services.
- `Block` unrequired ports.
- Tightly `control` the connecting of external storage devices and media (if it’s allowed at all).


> Hardening techniques must only be implemented to the extent that they meet security objectives, which must, in turn, align with organizational objectives. Security must always be balanced with both functionality and available resources <a href="#r4">(Rogers, Bobby)</a>.

> One of the most important ways that system administrators can protect endpoints is by hardening their configurations, making them as attack-resistant as possible. This includes disabling any unnecessary services or ports on the endpoints to reduce their susceptibility to attack, ensuring that secure configuration settings exist on devices and centrally controlling device security settings <a href="#r5">(Chapple & Seidl)</a>.

</details>


<details>
  <summary markdown="span">Compensating controls</summary>


<a href="https://csrc.nist.gov/glossary/term/compensating_controls">
<img title="compensating control" alt="compensating control" src="/images/compensating-control.png">
</a>

> Here’s an example: A small business processes credit card payments on its online store and is therefore subject to PCI DSS. This business uses the same network for both sensitive financial operations and external web access. Although best practices would dictate that physically separate infrastructures be used, with perhaps an air gap between the two, this may not be feasible because of cost. A compensating control would be to introduce a switch capable of VLAN management and to enforce ACLs at the switch and router levels. This alternative solution would “meet the intent and rigor of the original stated requirement,” as described in the PCI DSS standard <a href="#r1">(Chapman & Maymí)</a>.

> Three things must be considered when implementing a compensating control: vulnerability, threat, and risk. For example, a good compensating control might be to implement the appropriate access control list (ACL) and encrypt the data. The ACL protects the integrity of the data, and the encryption protects the confidentiality of the data <a href="#r3">(McMillan, Troy)</a>.

> A compensating control is an alternative security control put into place to compensate for any technical or business constraints placed upon a primary security control <a href="#r4">(Rogers, Bobby)</a>.

> For example, an organization may not be able to upgrade the operating system on retail point-of-sale (POS) terminals due to an incompatibility with the POS software. In these cases, security professionals should seek out compensating controls designed to provide a similar level of security using alternate means. In the POS example, administrators might place the POS terminals on a segmented, isolated network and use intrusion prevention systems to monitor network traffic for any attempt to exploit an unpatched vulnerability and block it from reaching the vulnerable host. This meets the same objective of protecting the POS terminal from compromise and serves as a compensating control <a href="#r5">(Chapple & Seidl)</a>.


</details>


<details>
  <summary markdown="span">Risk acceptance</summary>

> Every vulnerability management plan should include a strategy for dealing with accepted vulnerabilities. Business priorities, resource restrictions, or other issues may dictate that risk mitigation wait for another time. Although such risk acceptance is often reasonable and a fact of security life, accepted vulnerabilities should not simply be ignored <a href="#r1">(Chapman & Maymí)</a>.

> After an organization understands its risk, it must determine how to handle the risk. The following four basic methods are used to handle risk <a href="#r3">(McMillan, Troy)</a>:
- `Risk avoidance`: Terminating the activity that causes a risk or choosing an alternative that is not as risky
- `Risk transfer`: Passing on the risk to a third party, such as an insurance company
- `Risk mitigation`: Defining the acceptable risk level the organization can tolerate and reducing the risk to that level
- `Risk acceptance`: Understanding and accepting the level of risk as well as the cost of damages that can occur

> Never forget that the goal is to reduce risk to an acceptable level. Go any further, and you might harm the organization’s ability to perform crucial business tasks. If security negatively affects an organization’s business processes, that, in itself, increases the organization’s risk <a href="#r4">(Rogers, Bobby)</a>.

> Risk acceptance is a deliberate decision that comes as the result of a thoughtful analysis. It should not be undertaken as a default strategy. Simply stating that “we accept this risk” without analysis is not an example of an accepted risk; it is an example of an unmanaged risk <a href="#r5">(Chapple & Seidl)</a>!

</details>


<details>
  <summary markdown="span">Verification of mitigation</summary>


<a href="https://csrc.nist.gov/glossary/term/verification">
<img title="verification" alt="verification" src="/images/verification.png">
</a>

> Security controls may fail to protect information systems against threats for a variety of reasons. If the control is improperly installed or configured, or if you chose the wrong control to begin with, the asset will remain vulnerable. (You just won’t know it.) For this reason, you should create a formal procedure that describes the steps by which your organization’s security staff will verify and validate the controls they use. Verification is the process of ensuring that the control was implemented correctly. Validation ensures that the (correctly installed) control actually mitigates the intended threat <a href="#r1">(Chapman & Maymí)</a>.

> Once a threat has been remediated, you should verify that the mitigation has solved the issue. You should also take steps to ensure that all is back to its normal secure state. These steps validate that you are finished and can move on to taking corrective actions with respect to the lessons learned <a href="#r3">(McMillan, Troy)</a>.

> After we mitigate all known vulnerabilities, we should rerun our vulnerability scans to verify that previously identified vulnerabilities are gone and that no new vulnerabilities have been introduced. To be extra safe, verification may involve a separate auditing process to ensure remediated vulnerabilities are truly gone <a href="#r4">(Rogers, Bobby)</a>.


</details>

### Scanning parameters and criteria

<details>
  <summary markdown="span">Risk associated with scanning activities</summary>

> The risk appetite of an organization is the amount of risk that its senior executives are willing to assume <a href="#r1">(Chapman & Maymí)</a>.

> While vulnerability scanning is an advisable and valid process, there are some risks to note <a href="#r3">(McMillan, Troy)</a>:
- A false sense of security can be introduced because scans are not error free.
- Many tools rely on a database of known vulnerabilities and are only as valid as the latest update.
- Identifying vulnerabilities does not in and of itself reduce your risk or improve your security.

> Ironically, vulnerability scans not only help reduce organizational risk, but also introduce some risk of their own. For example, if too many vulnerability scanner plug-ins are selected—particularly the DoS variety—a scan could crash certain systems <a href="#r4">(Rogers, Bobby)</a>.


</details>


<details>
  <summary markdown="span">Vulnerability feed</summary>

> If your vulnerability feed is not one with a fast update cycle, or if you want to ensure you are absolutely abreast of the latest discovered vulnerabilities, you can (and perhaps should) subscribe to alerts in addition to those of your provider. The National Vulnerability Database (NVD) maintained by the National Institute of Standards and Technologies (NIST) provides two Rich Site Summary (RSS) feeds, one of which will alert you to any new vulnerability reported, and the other provides only those that have been analyzed. The advantage of the first feed is that you are on the bleeding edge of notifications. The advantage of the second is that it provides you with specific products that are affected as well as additional analysis <a href="#r1">(Chapman & Maymí)</a>.

> Vulnerability feeds are RSS feeds dedicated to the sharing of information about the latest vulnerabilities. Subscribing to these feeds can enhance the knowledge of the scanning team and can keep the team abreast of the latest issues <a href="#r3">(McMillan, Troy)</a>.

> SCAP was created by NIST as a way of standardizing the way vulnerability scanning practices are expressed, including automation, reporting, scoring, and prioritization of scans <a href="#r4">(Rogers, Bobby)</a>.

</details>


<details>
  <summary markdown="span">Scope</summary>

> Whether you are doing a global or targeted scan, your tools must know which nodes to test and which ones to leave alone. The set of devices that will be assessed constitutes the scope of the vulnerability scan. Deliberately scoping these events is important for a variety of reasons, but one of the most important ones is the need for credentials, which we discuss next <a href="#r1">(Chapman & Maymí)</a>.

> In the OpenVAS vulnerability scanner, you can set the scope by setting the plug-ins and the targets. Plug-ins define the scans to be performed, and targets specify the machines <a href="#r3">(McMillan, Troy)</a>.

>  Scope might also include time-of-day requirements and frequency to ensure scans minimize impact to the business <a href="#r4">(Rogers, Bobby)</a>.


</details>


<details>
  <summary markdown="span">Credentialed vs. non-credentialed</summary>

> Noncredentialed scans look at systems from the perspective of the attacker but are not as thorough as credentialed scans<a href="#r1">(Chapman & Maymí)</a>.

> A credentialed scan is a scan that is performed by someone with administrative rights to the host being scanned, while a non-credentialed scan is performed by someone lacking these rights <a href="#r3">(McMillan, Troy)</a>.

> Non-credentialed scans are easier, but easier doesn’t equate to more discovery and remediation of vulnerabilities. Avoid non-credentialed scans unless your goal is to simulate the vulnerability scanning perspective of the attacker <a href="#r4">(Rogers, Bobby)</a>.


</details>


<details>
  <summary markdown="span">Server-based vs. agent-based</summary>

> While both agent-based and agentless scanners are suitable for determining patch levels and missing updates, agent-based (or serverless) vulnerability scanners are typically better for scanning mobile devices <a href="#r1">(Chapman & Maymí)</a>.

The following Table by Author Troy McMillan define the server and agent based vulnerability scanners<a href="#r3">(McMillan, Troy)</a>:

<table>
  <tr>
    <th>Type</th>
    <th>Technology</th>
    <th>Character</th>
  </tr>
  <tr>
    <td>Agent based</td>
    <td>Pull technology</td>
    <td>Can get information from disconnected machines or machines in the DMZ Ideal for remote locations that have limited bandwidth Less dependent on network connectivity Based on policies defined on the central console</td>
  </tr>
  <tr>
    <td>Server based</td>
    <td>Push technology</td>
    <td>Good for networks with plentiful bandwidth Dependent on network connectivity Central authority does all the scanning and deployment</td>
  </tr>
</table>


> Agentless scans are often frowned upon due to the number of network resources they consume. You’ll be better off performing agentless scans during business off-hours <a href="#r4">(Rogers, Bobby)</a>.


</details>


<details>
  <summary markdown="span">Internal vs. external</summary>

> Many modern vulnerability scanning services allow for scanning of IP addresses on your network from an outside location or from within the corporate network. Internal scanners usually use appliances located within the network to perform the scanning activity, taking advantage of their privileged position to gain visibility of devices across the network. With an external scan, it’s possible to get a sense of what vulnerabilities exist from an outsider’s point of view <a href="#r1">(Chapman & Maymí)</a>.

> Troy McMillan proved the following definitions<a href="#r3">(McMillan, Troy)</a>:
- `external scan` A vulnerability scan performed from outside the organization’s network to assess the likelihood of an external attack.
- `internal scan` A vulnerability scan performed from inside the organization’s network to assess the likelihood of an insider attack.

> External scanning provides organizations with the “hacker viewpoint” of your network. Remediation of any vulnerable assets here should be a high priority <a href="#r4">(Rogers, Bobby)</a>.

> The internal and external scans required by PCI DSS are a good example of scans performed from different perspectives. The organization may conduct its own internal scans but must supplement them with external scans conducted by an approved scanning vendor <a href="#r5">(Chapple & Seidl)</a>.

</details>


<details>
  <summary markdown="span">Special considerations</summary>

> Apart from the considerations in a credentialed scan already discussed, the scanning tool must have the correct permissions on whichever hosts it is running, as well as the necessary access across the network infrastructure. It is generally best to have a dedicated account for the scanning tool or, alternatively, to execute it within the context of the user responsible for running the scan. In either case, minimally privileged accounts should be used to minimize risks (that is, do not run the scanner as root unless you have no other choice) <a href="#r1">(Chapman & Maymí)</a>.

> Just as the requirements of the vulnerability management program were defined in the beginning of the process, scanning criteria must be settled upon before scanning begins <a href="#r3">(McMillan, Troy)</a>.

<details>
  <summary markdown="span">Types of data</summary>

> Typical classification levels include the following <a href="#r1">(Chapman & Maymí)</a>:
- `Private`   Information whose improper disclosure could raise personal privacy issues
- `Confidential`   Data that could cause grave damage to the organization
- `Proprietary (or sensitive)`   Data that could cause some damage, such as loss of competitiveness to the organization
- `Public`   Data whose release would have no adverse effect on the organization

> Vulnerability scanners can usually scan more data types than anyone would need. As a result, you’ll typically want to condense the scanning of data to what is needed for regulatory or compliance purposes. Compliance-based scans will often compel you to scan sensitive or classified data types since vulnerabilities in these areas would need to be prioritized. You’ll also want to be mindful that scanning different data types, and the subsequent reporting, will be driven by different target audiences <a href="#r4">(Rogers, Bobby)</a>.


</details>

<details>
  <summary markdown="span">Technical constraints</summary>

> the term capacity is used to denote computational resources expressed in cycles of CPU time, bytes of primary and secondary memory, and bits per second (bps) of network connectivity. Because any scanning tool you choose to use will require a minimum amount of such capacity, you may be constrained in both the frequency and scope of your vulnerability scans<a href="#r1">(Chapman & Maymí)</a>.

> In some cases the scan will be affected by technical constraints. Perhaps the way in which you have segmented the network caused you to have to run the scan multiple times from various locations in the network. You will also be limited by the technical capabilities of the scan tool you use <a href="#r3">(McMillan, Troy)</a>.

> Another valuable NIST publication, SP 800-115, “Technical Guide to Information Security Testing and Assessment,” states that security assessments require resources such as time, staff, hardware, and software, and resource availability is often a limiting factor in the type of frequency of security assessments. In short, vulnerability scans can only be as effective as the technical resources afforded to them <a href="#r4">(Rogers, Bobby)</a>.

> Technical constraints may limit the frequency of scanning. For example, the scanning system may only be capable of performing a certain number of scans per day, and organizations may need to adjust scan frequency to ensure that all scans complete successfully<a href="#r5">(Chapple & Seidl)</a>.

</details>

<details>
  <summary markdown="span">Workflow</summary>

> Apart from well-written policies, the next best way to ensure this happens is by writing it into the daily workflows of security and IT personnel. If you work in a security operation center (SOC) and know that every Tuesday morning your duties include reviewing the vulnerability scans from the night before and creating tickets for any required remediation, then you’re much more likely to do this routinely <a href="#r1">(Chapman & Maymí)</a>.

> Workflow can also influence the scan. You might be limited to running scans at certain times because it negatively affects workflow. While security is important it isn’t helpful if it detracts from business processes that keep the organization in business<a href="#r3">(McMillan, Troy)</a>.

> The impact of vulnerability scanning to organizational workflows should’ve already been figured out earlier when we were talking about risks associated with vulnerability scanning <a href="#r4">(Rogers, Bobby)</a>.


</details>


<details>
  <summary markdown="span">Sensitivity levels</summary>

<a href="#r1">(Chapman & Maymí)</a>

> Sensitivity also refers to how deeply a scan probes each host. Scanning tools have templates that can be used to perform certain types of scans. These are two of the most common templates in use <a href="#r3">(McMillan, Troy)</a>:
- `Discovery scans`: These scans are typically used to create an asset inventory of all hosts and all available services.
- `Assessment scans`: These scans are more comprehensive than discovery scans and can identify misconfigurations, malware, application settings that are against policy, and weak passwords. These scans have a significant impact on the scanned device.

> If any systems have the following sensitive data types, it should be prioritized for detection and remediation <a href="#r4">(Rogers, Bobby)</a>:
- `Classified data` (confidential, secret, top secret)
- `Personally identifiable information (PII)` such as Social Security numbers or addresses
- `Financial information` such as credit card numbers or bank account information
- `Passwords`
- `Driver’s license or passport numbers`
- `Medical` information

> Administrators may also improve the efficiency of their scans by configuring the specific plug-ins that will run during each scan. Each plug-in performs a check for a specific vulnerability, and these plug-ins are often grouped into families based on the operating system, application, or device that they involve. Disabling unnecessary plug-ins improves the speed of the scan by bypassing unnecessary checks and also may reduce the number of false positive results detected by the scanner<a href="#r5">(Chapple & Seidl)</a>.

</details>


<details>
  <summary markdown="span">Regulatory requirements</summary>

  #### Some regulatory requirement links
  * [https://www.law.cornell.edu/wex/sarbanes-oxley_act](https://www.law.cornell.edu/wex/sarbanes-oxley_act)
  * [https://www.cdc.gov/phlp/publications/topic/hipaa.html](https://www.cdc.gov/phlp/publications/topic/hipaa.html)
  * [https://www.ftc.gov/tips-advice/business-center/privacy-and-security/gramm-leach-bliley-act](https://www.ftc.gov/tips-advice/business-center/privacy-and-security/gramm-leach-bliley-act)
  * [https://www.pcisecuritystandards.org/](https://www.pcisecuritystandards.org/)
  * [https://csrc.nist.gov/projects/risk-management/fisma-background](https://csrc.nist.gov/projects/risk-management/fisma-background)

> Does the organization operate in an industry that is regulated? If so, all regulatory requirements must be recorded, and the vulnerability assessment must be designed to support all requirements. The following are some examples of industries in which security requirements exist <a href="#r3">(McMillan, Troy)</a>:
- `Finance` (for example, banks and brokerages)
- `Medical` (for example, hospitals, clinics, and insurance companies)
- `Retail` (for example, credit card and customer information)

> Organizations are beholden to legal and regulatory requirements, which can vary based on country, state, or industry. Industry standards such as PCI DSS require vulnerability scans to be performed quarterly, after significant network changes take place, and by an approved vendor, for example, so we comply with the standard. Failing to abide by regulatory requirements can result in severe fines, business shutdown, and even the arrest of certain senior leadership members<a href="#r4">(Rogers, Bobby)</a>.


</details>


<details>
  <summary markdown="span">Segmentation</summary>

  <a href="https://www.cisco.com/c/en/us/products/security/what-is-network-segmentation.html">
<img title="segmentation" alt="segmentation" src="/images/cisco.png">
</a>

<a href="https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/lanswitch/configuration/12-2sx/lsw-12-2sx-book.pdf">
<img title="vlan segmentation" alt="vlan segmentation" src="/images/vlan.png">
</a>

> Typically reserved for the most extreme scenarios, air gapping provides the most restrictive type of segmentation. Air-gapped devices have zero network connectivity with anything. The most common way to exchange data with an air-gapped device is through a USB flash drive<a href="#r4">(Rogers, Bobby)</a>.

</details>

<details>
  <summary markdown="span">Intrusion prevention system (IPS), intrusion detection system (IPS), and firewall settings</summary>

   <a href="https://csrc.nist.gov/glossary/term/intrusion_detection_system">
<img title="ids" alt="ids" src="/images/ids.png">
</a>

   <a href="https://csrc.nist.gov/glossary/term/intrusion_prevention_system">
<img title="ips" alt="ips" src="/images/ips.png">
</a>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
> Author Troy McMillan provides the following definitions for IDS<a href="#r3">(McMillan, Troy)</a>:
- `Signature based`: This type of IDS analyzes traffic and compares it to attack or state patterns, called signatures, that reside within the IDS database.
- `Anomaly-based`: This type of IDS analyzes traffic and compares it to normal traffic to determine whether said traffic is a threat. 
- `Rule or heuristic based`: This type of IDS is an expert system that uses a knowledge base, an inference engine, and rule-based programming. The knowledge is configured as rules. The data and traffic are analyzed, and the rules are applied to the analyzed traffic. The inference engine uses its intelligent software to “learn.” When characteristics of an attack are met, they trigger alerts or notifications. This is often referred to as an IF/THEN, or expert, system.
</details>

</details>


### Inhibitors to remediation

<details>
  <summary markdown="span">Memorandum of understanding (MOU)</summary>

  <a href="https://csrc.nist.gov/glossary/term/memorandum_of_understanding_or_agreement">
<img title="moa" alt="moa" src="/images/moa.png">
</a>


</details>


<details>
  <summary markdown="span">Service-level agreement (SLA)</summary>

  <a href="https://csrc.nist.gov/glossary/term/service_level_agreement">
<img title="sla" alt="sla" src="/images/sla.png">
</a>

</details>


<details>
  <summary markdown="span">Organizational governance</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Business process interruption</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Degraded functionality</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Legacy systems</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Proprietary systems</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 1.4 Given a scenario, analyze the output from common vulnerability assessment tools.
### Web application scanner

<details>
  <summary markdown="span">OWASP Zed Attack Proxy (ZAP)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Burp suite</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Nikto</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Arachni</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Infastructure vulnerability scanner

<details>
  <summary markdown="span">Nessus</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">OpenVAS</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Qualys</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Software assessment tools and techniques

<details>
  <summary markdown="span">Static analysis</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Dynamic analysis</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Reverse engineering</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Fuzzing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Enumeration

<details>
  <summary markdown="span">Nmap</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">hping</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Active vs. passive</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Responder</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Wireless assessment tools

<details>
  <summary markdown="span">Aircrack-ng</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Reaver</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">oclHashcat</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Cloud Infrastructure assessment tools

<details>
  <summary markdown="span">ScoutSuite</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Prowler</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Pacu</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 1.5 Explain the threats and vulnerabilities associated with specialized technology.
### Mobile

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Internet of Things (IoT)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Embedded

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Real-time operating system (RTOS)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### System on Chip (SoC)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Field programmable gate array (FPGA)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Physical access control

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Building automation systems

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Vehicles and drones

<details>
  <summary markdown="span">CAN bus</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Workflow and process automation systems

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Industrial control system

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Supervisory control and data acquisition (SCADA)

<details>
  <summary markdown="span">Modbus</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 1.6 Explain the threats and vulnerabilities associated with operating in the cloud.
### Cloud service models

<details>
  <summary markdown="span">Software as a Service (SaaS)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Platform as a Service (PaaS)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Infastructure as a Service (IaaS)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Cloud deployment models

<details>
  <summary markdown="span">Public</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Private</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Community</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Hybrid</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Function as a Service (FaaS)/serverless architecture

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Infastructure as code (IaC)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Insecure application programming interface (API)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Improper key management

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Unprotected storage

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Logging and monitoring

<details>
  <summary markdown="span">Insufficient logging and monitoring</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Inability to access</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 1.7 Given a scenario, implement controls to mitigate attacks and software vulnerabilities.
### Attack types

<details>
  <summary markdown="span">Extensible markup language (XML) attack</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Structured query language (SQL) injection</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Overflow attack</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Buffer</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Integer</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Heap</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

</details>


<details>
  <summary markdown="span">Remote code execution</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Directory traversal</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Privilege escalation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Password spraying</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Credential stuffing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Impersonation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">On-path-attack (previously known as man-in-the-middle attack)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Session hijacking</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Rootkit</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Cross-site scripting</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Reflected</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Persistent</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Document object model (DOM)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>



</details>

### Vulnerabilities

<details>
  <summary markdown="span">Improper error handling</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Defeferencing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Insecure object reference</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Race condition</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Broken authentication</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Sensitive data exposure</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Insecure components</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Insufficient logging and monitoring</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Weak or default configurations</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Use of insecure functions</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">strcpy</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

</details>

# 2.0 Software and Systems Security
## 2.1 Given a scenario, apply security solutions for infrastructure management.
### Cloud vs. on-premises
### Asset management

<details>
  <summary markdown="span">Asset tagging</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Segmentation

<details>
  <summary markdown="span">Physical</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Virtual</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Jumpbox</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">System isolation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Air gap</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

</details>

### Network architecture

<details>
  <summary markdown="span">Physical</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Software-defined</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Virtual private cloud (VPC)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Virtual private network (VPN)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Serverless</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Change management
### Virtualization

<details>
  <summary markdown="span">Virtual desktop infastructure (VDI)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Containerization
### Identity and access management

<details>
  <summary markdown="span">Privilege management</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Multifactor authentication (MFA)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Single sign-on (SSO)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Federation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Role-based</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Attribute-based</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Mandatory</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Manual review</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Cloud access security security broker (CASB)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Honeypot

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Monitoring and logging

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Encryption

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Certificate management

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Active defense

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

## 2.2 Explain software assurance best practices.
### Platforms

<details>
  <summary markdown="span">Mobile</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Web application</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Client/server</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Embedded</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">System-on-chip (SoC)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Firmware</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Software development lifecycle (SDLC) integration

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### DevSecOps

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Software assessment methods

<details>
  <summary markdown="span">User acceptance testing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Stress test application</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Security regression testing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Code review</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Secure coding best practices

<details>
  <summary markdown="span">Input validation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Output encoding</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Session management</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Authentication</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data protection</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Parameterized queries</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Static analysis tools

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Dynamic analysis tools

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Formal methods for verification of critical software

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Service-oriented architecture

<details>
  <summary markdown="span">Security Assertions Markup Language (SAML)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Simple Object Access Protocol (SOAP)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Representation State Transfer (REST)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Microservices</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 2.3 Explain hardware assurance best practices.
### Hardware root of trust

<details>
  <summary markdown="span">Trusted platform module (TPM)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Hardware security module (HSM)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### eFuse

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Unified Extensible Firmware Interface (UEFI)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Trusted foundry

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Secure processing

<details>
  <summary markdown="span">Trusted execution</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Secure enclave</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Process security extensions</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Atomic execution</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Anti-tamper

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Self-encrypting drive

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Trusted firmware updates

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Measured boot and attestation

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Bus encryption

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

# 3.0 Security Operations and Monitoring.
## 3.1 Given a scenario, analyze data as part of security monitoring activities.
### Heuristics

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Trend analysis

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Endpoint

<details>
  <summary markdown="span">Malware</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Reverse engineering</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

</details>


<details>
  <summary markdown="span">Memory</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">System and application behavior</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Known-good behavior</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Anomalous behavior</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Exploit techniques</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

</details>


<details>
  <summary markdown="span">File system</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">User entity behavior analytics (UEBA)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Network

<details>
  <summary markdown="span">Uniform Resource Locator (URL) and domain name system (DNS) analysis</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Domain generation algorithm</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

</details>


<details>
  <summary markdown="span">Flow analysis</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Packet and protocol analysis</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Malware</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

</details>

### Log review

<details>
  <summary markdown="span">Event logs</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Syslog</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Firewall logs</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Web application firewall (WAF)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Proxy</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Intrusion detection system (IDS)/Intrusion prevention system(IPS)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Impact analysis

<details>
  <summary markdown="span">Organization impact vs. localized impact</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Immediate vs. total</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Security information and event management (SIEM) review

<details>
  <summary markdown="span">Rule writing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Known-bad Internet protocol (IP)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Dashboard</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Query writing

<details>
  <summary markdown="span">String search</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Script</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Piping</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### E-mail analysis

<details>
  <summary markdown="span">Malicious payload</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Domain Keys Identified Mail (DKIM)</summary>

* [https://dmarc.org/wiki/Glossary#A](https://dmarc.org/wiki/Glossary#A)

* [Provide Your Own DKIM Authentication Token in Amazon SES](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/send-email-authentication-dkim-bring-your-own.html)


* SPF (Sender Policy Framework) is a DNS text entry which shows a list of servers that should be considered allowed to send mail for a specific domain.
* DKIM (DomainKeys Identified Mail) should be instead considered a method to verify that the content of the messages is trustworthy, meaning that it wasn't changed from the moment the message left the initial mail server. Using encryption similar to MD5sum method.
* DMARC protects users by evaluating both SPF and DKIM and then determines if either domain matches the domain in the Display From address.

**DKIM (DomainKeys Identified Mail)** should be instead considered a method to verify that the content of the messages is trustworthy, meaning that it wasn't changed from the moment the message left the initial mail server. This additional layer of trustability is achieved by an implementation of the standard public/private key signing process. Once again the owners of the domain add a DNS entry with the public DKIM key which will be used by receivers to verify that the message DKIM signature is correct, while on the sender side the server will sign the entitled mail messages with the corresponding private key.

DKIM records are implemented as text records as well. The record must be created for a subdomain, which has a unique selector for that key, then a period (.), and then a protocol name `'_domainkey'` and the domain name itself. The type is TXT, and the value includes the type of key, followed by the actual key.

**DomainKeys Identified Message (DKIM)** is a signature-based Email Authentication technique. It is the result of merging the DomainKeys and Identified Internet Mail specifications, and has been published as a Standards Track document by the IETF as RFC4871 in 2007, and updated as RFC6376 in 2011. More information is available from DKIM.org or Wikipedia.

* DKIM provides a method for validating a domain name identity that is associated with a message through cryptographic authentication.
* DMARC uses DKIM results as one method (SPF being the other) for receivers to check email.

You can determine your domain's [DMARC alignment](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/send-email-authentication-dmarc.html) for DKIM by typing the following command at the command line, replacing example.com with your domain: 

```bash
nslookup -type=TXT _dmarc.example.com
```
In the output of this command, under Non-authoritative answer, look for a record that begins with `v=DMARC1`. If this record includes the string `adkim=r`, or if the adkim string is not present at all, then your domain uses relaxed alignment for DKIM. If the record includes the string `adkim=s`, then your domain uses strict alignment for DKIM. Your system administrator will need to remove this tag from the DMARC TXT record in your domain's DNS configuration. 

With Amazon SES you may configure DKIM authentication by using your own public-private key pair. This process is known as Bring Your Own DKIM (BYODKIM). With BYODKIM, you can use a single DNS record to configure DKIM authentication for your domains, as opposed to Easy DKIM, which requires you to publish three separate DNS records. Additionally, using BYODKIM lets you rotate the DKIM keys for your domains as often as you want. To use the Bring Your Own DKIM feature, you first have to create a key pair. The private key that you generate has to use 1024-bit RSA encoding. The private key has to be in PKCS #1 format. 

! At the command line, enter the following command to generate the private key: 

```bash
openssl genrsa -f4 -out private.key 1024
```

! At the command line, enter the following command to generate the public key: 

```bash
openssl rsa -in private.key -outform PEM -pubout -out public.key
```

! Now that you've created a key pair, you have to add the public key to the DNS configuration for your domain as a TXT record. A typical [DKIM record](https://www.namecheap.com/support/knowledgebase/article.aspx/317/2237/how-do-i-add-txtspfdkimdmarc-records-for-my-domain/) looks like the following:

```bash
selector1._domainkey.example.com        TXT     k=rsa;p=J8eTBu224i086iK
```



</details>


<details>
  <summary markdown="span">Domain-based Message Authentication, Reporting, and Conformance (DMARC)</summary>

* [https://dmarc.org/wiki/Glossary#A](https://dmarc.org/wiki/Glossary#A)
* [https://dmarc.org/](https://dmarc.org/)
* [https://datatracker.ietf.org/doc/html/rfc7489](https://datatracker.ietf.org/doc/html/rfc7489)

* SPF (Sender Policy Framework) is a DNS text entry which shows a list of servers that should be considered allowed to send mail for a specific domain.
* DKIM (DomainKeys Identified Mail) should be instead considered a method to verify that the content of the messages is trustworthy, meaning that it wasn't changed from the moment the message left the initial mail server. Using encryption similar to MD5sum method.
* DMARC protects users by evaluating both SPF and DKIM and then determines if either domain matches the domain in the Display From address.

**DMARC**, which stands for “Domain-based Message Authentication, Reporting & Conformance”, is an email authentication, policy, and reporting protocol. It builds on the widely deployed SPF and DKIM protocols, adding linkage to the author (“From:”) domain name, published policies for recipient handling of authentication failures, and reporting from receivers to senders, to improve and monitor protection of the domain from fraudulent email.

**Domain-based Message Authentication, Reporting, and Conformance (DMARC)** is a technical specification that allows Message Senders and Message Receivers to cooperate and thereby better detect when messages don't actually originate from the Internet domain they claim to have been sent from. It does this by allowing the Domain Owner to indicate they are using email authentication on the messages they send, optionally requesting that messages that fail to authenticate be blocked. Message Receivers honor these requests (unless there is a local policy overriding this action), and send reports on all the messages - whether or not they pass email authentication - to the Domain Owner. For more information please review the DMARC Overview. The specification has been published as an Informational document by the IETF as RFC7489.

[Resource Records](https://www.cisco.com/c/en/us/support/docs/ip/domain-name-system-dns/12684-dns-resource.html) define data types in the Domain Name System (DNS). Resource Records identified by RFC 1035 icon_popup_short.gif are stored in binary format internally for use by DNS software. But resource records are sent across a network in text format while they perform zone transfers.

[DMARC policies](https://dmarc.org/overview/) are published in the DNS as text (TXT) resource records (RR) and announce what an email receiver should do with non-aligned mail it receives. Consider an example DMARC TXT RR for the domain “sender.dmarcdomain.com” that reads:

```html
"v=DMARC1;p=reject;pct=100;rua=mailto:postmaster@dmarcdomain.com"
```

In this example, the sender requests that the receiver outright reject all non-aligned messages and send a report, in a specified aggregate format, about the rejections to a specified address. If the sender was testing its configuration, it could replace “reject” with “quarantine” which would tell the receiver they shouldn’t necessarily reject the message, but consider quarantining it.

<img src="images/dmarc.png" alt="dmarc">

DMARC (Domain-based Messaging and Reporting Compliance) is a technology designed to combat email spoofing and is useful to stop phishing. Specifically, it protects the case where a phisher has spoofed the Display From address (also know as 5322.From email address). DMARC protects users by evaluating both SPF and DKIM and then determines if either domain matches the domain in the Display From address. A very basic [DMARC record looks like the following](https://www.namecheap.com/support/knowledgebase/article.aspx/317/2237/how-do-i-add-txtspfdkimdmarc-records-for-my-domain/):

```html
_dmarc.example.com   TXT     v=DMARC1;p=none;sp=quarantine;pct=100;rua=mailto:dmarcreports@example.com
```
</details>


<details>
  <summary markdown="span">Sender Policy Framework (SPF)</summary>

* [https://dmarc.org/wiki/Glossary#A](https://dmarc.org/wiki/Glossary#A)
* [https://fraudmarc.com/what-is-spf/](https://fraudmarc.com/what-is-spf/)


* SPF (Sender Policy Framework) is a DNS text entry which shows a list of servers that should be considered allowed to send mail for a specific domain.
* DKIM (DomainKeys Identified Mail) should be instead considered a method to verify that the content of the messages is trustworthy, meaning that it wasn't changed from the moment the message left the initial mail server. Using encryption similar to MD5sum method.
* DMARC protects users by evaluating both SPF and DKIM and then determines if either domain matches the domain in the Display From address.

**Sender Policy Framework (SPF)** , originally Sender Permitted From, is a path-based Email Authentication technique. It was published as a Experimental document by the IETF as RFC4408 in 2006, and updated as a Standards Track document as RFC7208 in 2014. More information is available from OpenSPF.org or Wikipedia.

* SPF provides a method for validating the envelope sender domain identity that is associated with a message through path-based authentication.
* DMARC uses SPF results as one method (DKIM being the other) for receivers to check email.

You can determine your domain's [DMARC alignment](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/send-email-authentication-dmarc.html) for SPF by typing the following command at the command line, replacing example.com with your domain: 

```bash
nslookup -type=TXT _dmarc.example.com
```
In the output of this command, under Non-authoritative answer, look for a record that begins with `v=DMARC1`. If this record includes the string `aspf=r`, or if the aspf string is not present at all, then your domain uses relaxed alignment for SPF. If the record includes the string `aspf=s`, then your domain uses strict alignment for SPF. Your system administrator will need to remove this tag from the DMARC TXT record in your domain's DNS configuration. 

<img src="images/spf.png" alt="spf">

[SPF (Sender Policy Framework)](https://www.namecheap.com/support/knowledgebase/article.aspx/317/2237/how-do-i-add-txtspfdkimdmarc-records-for-my-domain/) is a DNS text entry which shows a list of servers that should be considered allowed to send mail for a specific domain. Incidentally the fact that SPF is a DNS entry can also considered a way to enforce the fact that the list is authoritative for the domain, since the owners/administrators are the only people allowed to add/change that main domain zone. Thus, SPF gives other mailservers a way to verify that mail claiming to be from your domain is sent from one of your authorized IP addresses. They do this by checking a special TXT record configured in the domain name zone. It helps to establish the legitimacy of the domain mail server and reduces the chances of spoofing, which occurs when someone fakes the headers on an email to make it look like it’s coming from your domain, even though the message did not originate from your mail server. A very basic SPF record looks like the following:

```html
example.com   TXT     v=spf1 a ~all
```
</details>


<details>
  <summary markdown="span">Phishing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Forwarding</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Digital signature</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">E-mail signature block</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Embedded links</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Impersonation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Header</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 3.2 Given a scenario, implement configuration changes to existing controls to improve security.
### Permissions

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Allow list (previously know as whitelisting)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Blocklist (previously known as blacklisting)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Firewall

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Intrusion prevention system (IPS) rules

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Data loss prevention (DLP)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Endpoint detection and response (EDR)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Network access control (NAC)

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Sinkholing

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Malware signatures

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Development/rule writing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Sandboxing

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Port security

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

## 3.3 Explain the importance of proactive threat hunting.
### Establishing a hypothesis

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Profiling threat actors and activities

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Threat hunting tactics

<details>
  <summary markdown="span">Executable process analysis</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Reducing the attack surface area

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Bundling critical assets

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Attack vectors

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Integrated intelligence

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Improving detection capabilities

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

## 3.4 Compare and contrast automation concepts and technologies.
### Workflow orchestration

<details>
  <summary markdown="span">Security Orchestration, Automation, and Response (SOAR)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Scripting 

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Application programming interface (API) integration

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Automated malware signature creation

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Data enrichment

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Threat feed combination

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Machine learning

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Use of automation protocols and standards

<details>
  <summary markdown="span">Security Content Automation Protocol (SCAP)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Continuous integration
### Continuous deployment/delivery
# 4.0 Incident Response
## 4.1 Explain the importance of the incident response process.
### Communication plan

<details>
  <summary markdown="span">Limiting communication to trusted parties</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Disclosing based on regulatory/legislative requirements</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Preventing inadvertent release of information</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Using a secure method of communication</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Reporting requirements</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Response coordination with relevant entities

<details>
  <summary markdown="span">Legal</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Human resources</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Public relations</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Internal and external</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Law enforcement</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Senior leadership</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Regulatory bodies</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Factors contributing to data criticality

<details>
  <summary markdown="span">Personally identifiable Information (PII)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Personal health information (PHI)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Personal health information (SPI)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">High value asset</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Financial information</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Intellectual property</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Corporate information</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 4.2 Given a scenario, apply the appropriate incident response procedure.
### Preparation

<details>
  <summary markdown="span">Training</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Testing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Documentation of procedures</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Detection and analysis

<details>
  <summary markdown="span">Characteristics contributing to severity level classification</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Downtime</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Recovery time</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data integrity</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Economic</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">System process criticality</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Reverse engineering</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data correlation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Containment

<details>
  <summary markdown="span">Segmentation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Isolation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Eradication and recovery

<details>
  <summary markdown="span">Vulnerability mitigation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Sanitization</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Reconstruction/reimaging</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Secure disposal</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Patching</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Restoration of permissions</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Reconstitution of resources</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Restoration of capabilities and services</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Verfication of logging/communication to security monitoring</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Post-incident activities

<details>
  <summary markdown="span">Evidence retention</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Lessons learned report</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Change control process</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Incident response plan update</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Incident summary report</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">IoC generation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Monitoring</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 4.3 Given an incident, analyze potential indicators of compromise.
### Network-related

<details>
  <summary markdown="span">Bandwidth consumption</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Beaconing</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Irregular peer-to-peer communication</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Rogue device on the network</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Scan/sweep</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Unusual traffic spike</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Common protocol over non-standard port</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Host-related

<details>
  <summary markdown="span">Processor consumption</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Memory consumption</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Drive capacity consumption</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Unauthorized software</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Malicious process</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Unauthorized change</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Unauthorized privilege</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data exfiltration</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Abnormal OS process behavior</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">File system change or anomaly</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Registry change or anomaly</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Unauthorized scheduled task</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Application-related

<details>
  <summary markdown="span">Anomalous activity</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Introduction of-new accounts</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Unexpected output</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Unexpected outbound communication</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Service interruption</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Application log</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 4.4 Given a scenario, utilize basic digital forensics techniques.
### Network

<details>
  <summary markdown="span">Wireshark</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">tcpdump</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Endpoint

<details>
  <summary markdown="span">Disk</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Memory</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Mobile

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Cloud

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Virtualization

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Legal hold

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Procedures

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Hashing

<details>
  <summary markdown="span">Changes to binaries</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Carving

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Data acquisition

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

# 5.0  Compliance and Assessment.
## 5.1 Understand the importance of data privacy and protection.
### Privacy vs. security

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Non-technical controls

<details>
  <summary markdown="span">Classification</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Ownership</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Retention</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data types</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Retention standards</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Confidentiality</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Legal requirements</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data sovereignty</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data minimization</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Purpose limitation</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Non-disclosure agreement (NDA)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Technical controls

<details>
  <summary markdown="span">Encryption</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data loss prevention (DLP)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data masking</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Deidentification</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Tokenization</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Digital rights management (DRM)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Watermarking</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

</details>


<details>
  <summary markdown="span">Geographic access requirements</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Access control</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 5.2 Given a scenario, apply security concepts in support of organizational risk mitigation.
### Business impact analysis

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Risk identification process

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Risk calculation

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>


<details>
  <summary markdown="span">Probability</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Magnitude</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Communication of risk factors

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Risk prioritization

<details>
  <summary markdown="span">Security controls</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Engineering tradeoffs</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### System assessment

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Documented compensating controls

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

### Training and exercises

<details>
  <summary markdown="span">Red team</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Blue team</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">White team</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Tabletop exercise</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Supply chain assessment

<details>
  <summary markdown="span">Vendor due diligence</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Hardware source authenticity</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

## 5.3 Explain the importance of frameworks, policies, procedures, and controls.
### Frameworks

<details>
  <summary markdown="span">Risk-based</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Prescriptive</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Policies and procedures

<details>
  <summary markdown="span">Code of conduct/ethics</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Accepatble use policy (AUP)</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Password policy</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data ownership</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Data retention</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Account management</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Continuous monitoring</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Work product retention</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Control types

<details>
  <summary markdown="span">Managerial</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Operational</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Technical</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Preventative</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Detective</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Responsive</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

<details>
  <summary markdown="span">Corrective</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

### Audits and assessments

<details>
  <summary markdown="span">Regulatory</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>


<details>
  <summary markdown="span">Compliance</summary>

<a href="#r1">(Chapman & Maymí)</a>
<a href="#r2">(Firts.org)</a>
<a href="#r3">(McMillan, Troy)</a>
<a href="#r4">(Rogers, Bobby)</a>
<a href="#r5">(Chapple & Seidl)</a>

</details>

# References

<A NAME=r1> 2021. Chapman, Brent, and Fernando Maymí. COMPTIA CYSA+ CYBERSECURITY ANALYST CERTIFICATION EXAM GUIDE (EXAM CS0-002). NEW YORK: MCGRAW-HILL EDUCATION. [https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260464313/](https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260464313/)


<A NAME=r2> Firts.org. 2012. Common Vulnerability Scoring System v3.1: User Guide. [https://www.first.org/cvss/v3.1/user-guide](https://www.first.org/cvss/v3.1/user-guide)


<A NAME=r3>McMillan, Troy</A> 2020. CompTIA Cybersecurity Analyst (CySA+) CS0-002 Cert Guide, 2nd Edition. Pearson IT Certification. [https://learning.oreilly.com/library/view/comptia-cybersecurity-analyst/9780136747000/](https://learning.oreilly.com/library/view/comptia-cybersecurity-analyst/9780136747000/)

<A NAME=r4>Rogers, Bobby</A> 2021. CompTIA CySA+ Cybersecurity Analyst Certification Passport (Exam CS0-002). [https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260462258/](https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260462258/)

<A NAME=r5> 2020. Chappel, Mike and David Seidl.</A>CompTIA CySA+ Study Guide Exam CS0-002, 2nd Edition.[https://learning.oreilly.com/library/view/comptia-cysa-study/9781119684053/](https://learning.oreilly.com/library/view/comptia-cysa-study/9781119684053/)

<A NAME=r6> 2020. Chapple, Mike, and David Seidl.</A> CompTIA CySA+ Practice Tests, 2nd Edition.[https://learning.oreilly.com/library/view/comptia-cysa-study/9781119684053/](https://learning.oreilly.com/library/view/comptia-cysa-study/9781119684053/) 
 
<A NAME=r7> Sparks, Kelly. 2020. CompTIA CySA+ Cybersecurity Analyst Certification Practice Exams (Exam CS0-002), 2nd Edition. [https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260473643/](https://learning.oreilly.com/library/view/comptia-cysa-cybersecurity/9781260473643/)

<A NAME=r8> Baker, David, Christey, Steven, Hill, William. The MITRE Corporation. 1999.The Development of a Common Enumeration of Vulnerabilities and Exposures.[https://cve.mitre.org/docs/docs-2001/Development_of_CVE.html](https://cve.mitre.org/docs/docs-2001/Development_of_CVE.html)

<A NAME=r9>Lockheed Martin Corp</A>Hutchins, Eric, Cloppert, Michael, Amin, Rohan.[Intelligence-Driven Computer Network Defense Informed by Analysis of Adversary Campaigns andIntrusion Kill Chains](https://www.lockheedmartin.com/content/dam/lockheed-martin/rms/documents/cyber/LM-White-Paper-Intel-Driven-Defense.pdf)

<A NAME=w1> Wikipedia contributors. (2021, September 14). Zero-day (computing). In Wikipedia, The Free Encyclopedia. Retrieved 11:58, October 17, 2021, from [https://en.wikipedia.org/w/index.php?title=Zero-day_(computing)&oldid=1044324299](https://en.wikipedia.org/wiki/Zero-day_(computing))
